/*
 * usb.h
 *
 * Created: 23/02/2014 05:09:30
 *  Author: VendWatch
 */ 


#ifndef USB_H_
#define USB_H_

#include "wiring.h"

void usb_begin(void);
bool usb_attached(void);

#endif /* USB_H_ */