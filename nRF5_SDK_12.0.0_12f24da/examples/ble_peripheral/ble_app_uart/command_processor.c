#include "command_processor.h"

#include "config.h"
#include "sdk_config.h"

#include "wiring.h"

#include "usb.h"
#include "serial.h"
#include "ble_uart.h"
#include "rgbled.h"
#include "ucs.h"
#include "fuelgauge.h"
#include "eeprom.h"
#include "accelerometer.h"
#include "settings.h"

#define LENBUFF 128
int xbuff = 0x00;
char cbuff[LENBUFF];
char rcvchar = 0x00;
int flagcommand = 0;

#define BOOTLOADER_DFU_START 0xB1 //0xB1

void inicbuff(void);
int addcbuff(char c);

void execute_command(void);

void inicbuff(void)
{
    int i;
    for(i=0;i<LENBUFF;i++)
    {
        cbuff[i]=0x00;
    }
    xbuff=0x00;
}

int addcbuff(char c)
{
    switch(c)
    {
        case 0x04: // Flag Enable
            flagcommand=1;
            break;
        case 0x08: // Delete
            if(xbuff>0) cbuff[--xbuff]=0x00;
            break;
        case 0x01B:
            inicbuff();
            break;
        default:
            cbuff[xbuff++]=c;
    }
    return (xbuff-1);
}

void execute_command(void)
{
    int i;

    char arg[LENBUFF];
    
    int recognized = 0;
    
    flagcommand=0;
    
    for(i=0;i<LENBUFF;i++){
            arg[i]=0x00;
    }

    if(cbuff[0]=='\\' && cbuff[1]=='s' && cbuff[2]=='b')
    {
        recognized = 1;

        i=3;
        do{
        arg[i-3]=cbuff[i];
        }while(cbuff[++i]!=0x00);

        static char inputS[4];
        inputS[0] = arg[0];
        inputS[1] = arg[1];
        inputS[2] = arg[2];
        inputS[3] = '\0';
        int inputN = atoi(inputS);
        settings_storage.led_brightness = inputN;
        settings_save_config();
        settings_load_config();
        ble_write_bytes("[ok]",4);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='s' && cbuff[2]=='t')
    {
        recognized = 1;
        i=3;
        do{ // Extraemos argumento del buffer
                arg[i-3]=cbuff[i]; // a partir del 3er byte y hasta \0.
        }while(cbuff[++i]!=0x00);

        static char inputS[6];
        inputS[0] = arg[0];
        inputS[1] = arg[1];
        inputS[2] = arg[2];
        inputS[3] = arg[3];
        inputS[4] = arg[4];
        inputS[5] = '\0';
        int inputN = atol(inputS);
        settings_storage.time_to_sleep = inputN;
        settings_save_config();
        settings_load_config();
        ble_write_bytes("[ok]",4);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        //stopwatch_setElapsed(settings_storage.time_to_sleep*(long)1000+(long)1000); //overflow limit time
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='s' && cbuff[2]=='r')
    {
        recognized = 1;
        i=3;
        do{ // Extraemos argumento del buffer
                arg[i-3]=cbuff[i]; // a partir del 3er byte y hasta \0.
        }while(cbuff[++i]!=0x00);

        static char inputS[6];
        inputS[0] = arg[0];
        inputS[1] = arg[1];
        inputS[2] = arg[2];
        inputS[3] = arg[3];
        inputS[4] = arg[4];
        inputS[5] = '\0';
        int inputN = atol(inputS);
        settings_storage.time_to_reconnect = inputN;
        settings_save_config();
        settings_load_config();
        ble_write_bytes("[ok]",4);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        //stopwatch_setElapsed(settings_storage.time_to_sleep*(long)1000+(long)1000); //overflow limit time
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='s' && cbuff[2]=='a')
    {
        recognized = 1;
        i=3;
        do{ // Extraemos argumento del buffer
                arg[i-3]=cbuff[i]; // a partir del 3er byte y hasta \0.
        }while(cbuff[++i]!=0x00);
        static char inputS[4];
        inputS[0] = arg[0];
        inputS[1] = arg[1];
        inputS[2] = arg[2];
        inputS[3] = '\0';
        int inputN = atoi(inputS);
        //trace_printf("SET [Wake on Shake Sensitivity] = %d\r\n", inputN);
        settings_storage.wake_on_shake_sensitivity = inputN;
        settings_save_config();
        settings_load_config();
        accelerometer_shakeMode(settings_storage.wake_on_shake_sensitivity, true, true, true, false);
        ble_write_bytes("[ok]",4);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='b')
    {
        recognized = 1;
        ble_write_bytes("[gb",3);
        ble_do_events();
        char buffer[4];
        sprintf(buffer,"%03d", settings_storage.led_brightness);
        ble_write_bytes(buffer,3);
        ble_do_events();
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='t')
    {
        recognized = 1;
        ble_write_bytes("[gt",3);
        ble_do_events();
        char buffer[6];
        sprintf(buffer,"%05lu", settings_storage.time_to_sleep);
        ble_write_bytes(buffer,5);
        ble_do_events();
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='r')
    {
        recognized = 1;
        ble_write_bytes("[gr",3);
        ble_do_events();
        char buffer[6];
        sprintf(buffer,"%05lu", settings_storage.time_to_reconnect);
        ble_write_bytes(buffer,5);
        ble_do_events();
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='a')
    {
        recognized = 1;
        ble_write_bytes("[ga",3);
        ble_do_events();
        char buffer[4];
        sprintf(buffer,"%03d", settings_storage.wake_on_shake_sensitivity);
        ble_write_bytes(buffer,3);
        ble_do_events();
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='s')
    {
        recognized = 1;
        float varSoC = fuelgauge_getSoC();
        ble_write_bytes("[gs",3);
        ble_do_events();
        char buffer[7];
        sprintf(buffer, "%03.2f", varSoC);
        ble_write_bytes(buffer,6);
        ble_do_events();
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }

    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='v')
    {
        recognized = 1;
        float varVCell = fuelgauge_getVCell();
        ble_write_bytes("[gv", 3);
        ble_do_events();
        char buffer[7];
        sprintf(buffer, "%03.2f", varVCell);
        ble_write_bytes(buffer, 6);
        ble_do_events();
        ble_write_bytes("]", 1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='r' && cbuff[2]=='v')
    {
        recognized = 1;
        ble_write_bytes("[rv", 3);
        ble_do_events();
        char buffer[10];
        sprintf(buffer, "%s", settings_storage.version_of_program);
        ble_write_bytes(buffer, 9);
        ble_do_events();
        ble_write_bytes("]", 1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='u')
    {
        recognized = 1;
        ble_write_bytes("[gu",3);
        ble_do_events();
        char buffer[2];
        sprintf(buffer, "%s", usb_attached()?"T":"F");
        ble_write_bytes(buffer,1);
        ble_do_events();
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='p')
    {
        recognized = 1;
        ble_write_bytes("[gp",3);
        ble_do_events();
        char buffer[2];

        sprintf(buffer, "%s", serial_attached()?"T":"F");

        ble_write_bytes(buffer,1);
        ble_do_events();

        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        
        inicbuff();
    }
    
   
    
     if(cbuff[0]=='\\' && cbuff[1]=='x' && cbuff[2]=='d')
    {
        recognized = 1;
        ble_write_bytes("[xdb]", 5);
        ble_do_events();
        
        ble_write(0x04);
        ble_do_events();
        
        serial_begin();
        ucs_readDexMethod();
        serial_end();
        	
        ble_write_bytes("[xde]", 5);
        ble_do_events();
        
        ble_write(0x04);
        ble_do_events();
        
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='m')
    {
        recognized = 1;
        rgbled_set(0,0,settings_storage.led_brightness);
        ble_write_bytes("[gmb]",5);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        
        ucs_readDexData(0, dexLength);
       
        ble_write(0x04);
        ble_do_events();
        ble_write_bytes("[gme]",5);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        rgbled_set(0,0,settings_storage.led_brightness);
        
        if(dexLength>0)
        {
            rgbled_set(0,settings_storage.led_brightness,0);
            delay(250);
            rgbled_set(0,0,0);
            delay(250);
            rgbled_set(0,settings_storage.led_brightness,0);
            delay(250);
        }
        else
        {
            rgbled_set(settings_storage.led_brightness, 0, 0);
            delay(250);
            rgbled_set(0,0,0);
            delay(250);
            rgbled_set(settings_storage.led_brightness, 0, 0);
            delay(250);
        }
        
        rgbled_set(0,0,0);
        delay(250);
        rgbled_set(0, 0,settings_storage.led_brightness);
        delay(250);
        
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='s' && cbuff[2]=='w')
    {

            recognized = 1;

            

            i=3;

            do{ // Extraemos argumento del buffer

                    arg[i-3]=cbuff[i]; // a partir del 3er byte y hasta \0.

            }while(cbuff[++i]!=0x00);

            

            static char inputS[1];

            inputS[0] = arg[0];

            //inputS[1] = '\0';

            int inputN = atoi(inputS);

            

            settings_storage.wake_on_serial_switch = inputN;

            settings_save_config();
            settings_load_config();

            

            ble_write_bytes("[ok]",4);

            ble_do_events();

            

            ble_write(0x04);

            ble_do_events();

            

            inicbuff();
    }

	

    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='w')

    {

            recognized = 1;

            ble_write_bytes("[gw",3);

            ble_do_events();

            

            char buffer[1];

            sprintf(buffer,"%d", settings_storage.wake_on_serial_switch);

            

            ble_write_bytes(buffer,1);

            ble_do_events();

            

            ble_write_bytes("]",1);

            ble_do_events();

            

            ble_write(0x04);

            ble_do_events();

            

            inicbuff();

    }
    
    /////////////////////////////////////////////////////////////////

    if(cbuff[0]=='\\' && cbuff[1]=='s' && cbuff[2]=='c')

    {

            recognized = 1;

            

            i=3;

            do{ // Extraemos argumento del buffer

                    arg[i-3]=cbuff[i]; // a partir del 3er byte y hasta \0.

            }while(cbuff[++i]!=0x00);

            

            static char inputS[1];

            inputS[0] = arg[0];

            //inputS[1] = '\0';

            int inputN = atoi(inputS);

            

            settings_storage.wake_on_charging = inputN;

            settings_save_config();
            settings_load_config();

            

            ble_write_bytes("[ok]",4);

            ble_do_events();

            

            ble_write(0x04);

            ble_do_events();

            

            inicbuff();

    }

    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='c')

    {

            recognized = 1;

            ble_write_bytes("[gc",3);

            ble_do_events();

            

            char buffer[1];

            sprintf(buffer,"%d", settings_storage.wake_on_charging);

            

            ble_write_bytes(buffer,1);

            ble_do_events();

            

            ble_write_bytes("]",1);

            ble_do_events();

            

            ble_write(0x04);

            ble_do_events();

            

            inicbuff();

    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='s' && cbuff[2]=='s')
    {
        char buffer[7];
        recognized = 1;
        ble_write_bytes("[ss",3);
        ble_do_events();
        //////////////////////////////////////////////////////
        // 1. Time To Sleep
        sprintf(buffer,"%05lu", settings_storage.time_to_sleep);
        ble_write_bytes(buffer,5);
        ble_do_events();
        /////////////////////
        ble_write_bytes(",",1);
        ble_do_events();
        /////////////////////
        
        // 2. Time To Reconnect
        sprintf(buffer,"%05lu", settings_storage.time_to_reconnect);
        ble_write_bytes(buffer,5);
        ble_do_events();
        /////////////////////
        ble_write_bytes(",",1);
        ble_do_events();
        /////////////////////
        
        // 3. Led Brightness
        sprintf(buffer,"%03d", settings_storage.led_brightness);
        ble_write_bytes(buffer,3);
        ble_do_events();
        /////////////////////
        ble_write_bytes(",",1);
        ble_do_events();
        /////////////////////
        // 4. Shake to Wake
        sprintf(buffer,"%03d", settings_storage.wake_on_shake_sensitivity);
        ble_write_bytes(buffer,3);
        ble_do_events();
        /////////////////////
        ble_write_bytes(",",1);
        ble_do_events();
        /////////////////////
        // 5. Plug to Wake
        sprintf(buffer,"%d", settings_storage.wake_on_serial_switch);
        ble_write_bytes(buffer,1);
        ble_do_events();
        /////////////////////
        ble_write_bytes(",",1);
        ble_do_events();
        /////////////////////
        // 6. Wake while charging
        sprintf(buffer,"%d", settings_storage.wake_on_charging);
        ble_write_bytes(buffer,1);
        ble_do_events();
        /////////////////////
        //ble_write_bytes(",",1);
        //ble_do_events();
        /////////////////////
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='h' && cbuff[2]=='s')
    {
        char buffer[7];
        recognized = 1;
        ble_write_bytes("[hs",3);
        ble_do_events();
        //////////////////////////////////////////////////////
        // 1 Battery SoC
        float varSoC = fuelgauge_getSoC();
        sprintf(buffer, "%03.2f", varSoC);
        ble_write_bytes(buffer,6);
        ble_do_events();
        /////////////////////
        ble_write_bytes(",",1);
        ble_do_events();
        /////////////////////
        // 2 Voltage
        float varVCell = fuelgauge_getVCell();
        sprintf(buffer, "%03.2f", varVCell);
        ble_write_bytes(buffer,6);
        ble_do_events();
        /////////////////////
        ble_write_bytes(",",1);
        ble_do_events();
        /////////////////////
        // 3 USB Attached
        sprintf(buffer, "%s", usb_attached()?"T":"F");
        ble_write_bytes(buffer,1);
        ble_do_events();
        /////////////////////
        ble_write_bytes(",",1);
        ble_do_events();
        /////////////////////
        // 4 Serial Attached
        sprintf(buffer, "%s", serial_attached()?"T":"F");
        ble_write_bytes(buffer,1);
        ble_do_events();
        //////////////////////////////////////////////////////
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        rgbled_set(0,0,settings_storage.led_brightness);
        
        inicbuff();
    }
    
   
    
    if(cbuff[0]=='\\' && cbuff[1]=='x' && cbuff[2]=='s' && cbuff[3]=='d' && cbuff[4]=='(')
    {
        recognized = 1;

        // password 13

        char strPass[14] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        char* pCommandData;

        int i=0;

        for(i=0; i<14; i++)
        {
            if(cbuff[5+i]==',')break;
            strPass[i] = cbuff[5+i];
        }

        strPass[i+1]='\0';

        pCommandData = &cbuff[5+i+1];

        // Now, find the end of the command data..

        int j = 0;
        for(j=0; j<LENBUFF - i; j++)
        {
            if( cbuff[j+(5+i+1)] == ')' ) break;
        }

        *(pCommandData + j) = '\0';

        ble_write_bytes("[xsdb]",6);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();

        delay(10);

        /////////////////////////////////////////////////
        serial_begin();
        serial_wakeup();

        // Got to do this first or else our ack structures will not be initialized..

        UCS_Initialize();

        int status_sd= UCS_send_data(strPass, pCommandData);
        *(pCommandData + j) = ')';

        delay(150);

        serial_sleep();
        serial_end();

        char r[10];

        sprintf(r,"[xsds%02d]", status_sd);

        ble_write_bytes(r, 8);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();

        delay(10);

        /////////////////////////////////////////////////

        ble_write_bytes("[xsde]",6);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        delay(10);

        
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='i')
    {
        recognized = 1;
        ble_write_bytes("[gi",3);
        ble_do_events();
        char buffer[7];
        //sprintf(buffer,"%03d", settings_storage.led_brightness);
        buffer[0] = eeprom_read(0xfa);
        buffer[1] = eeprom_read(0xfb);
        buffer[2] = eeprom_read(0xfc);
        buffer[3] = eeprom_read(0xfd);
        buffer[4] = eeprom_read(0xfe);
        buffer[5] = eeprom_read(0xff);
        ble_write_bytes(buffer,6);
        ble_do_events();
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='d')
    {
        recognized = 1;
        ble_disconnect();
        //ble_write_bytes("*",1);
        //ble_do_events();
        //ble_write(0x04);
        //ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='r')
    {
        recognized = 1;
        //sd_power_gpregret_set(BOOTLOADER_DFU_START);
        NRF_POWER->GPREGRET = BOOTLOADER_DFU_START;//General purpose retention register
        NVIC_SystemReset();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='s' && cbuff[2]=='x')
    {
        recognized = 1;
        i=3;
        
        do{ // Extraemos argumento del buffer
                arg[i-3]=cbuff[i]; // a partir del 3er byte y hasta \0.
        }while(cbuff[++i]!=0x00);
        
        static char inputS[1];
        inputS[0] = arg[0];
        //inputS[1] = '\0';
        
        int inputN = atoi(inputS);
        settings_storage.bootloader_switch = inputN;
        NRF_POWER->GPREGRET = inputN;
        settings_save_config();
        settings_load_config();
        
        ble_write_bytes("[ok]",4);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='x')
    {
        recognized = 1;
        ble_write_bytes("[gx",3);
        ble_do_events();
        char buffer[1];
        sprintf(buffer,"%d", settings_storage.bootloader_switch);
        ble_write_bytes(buffer,1);
        ble_do_events();
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='s' && cbuff[2]=='q')
    {
        recognized = 1;
        //i=3;
        
        /*do{ // Extraemos argumento del buffer
                arg[i-3]=cbuff[i]; // a partir del 3er byte y hasta \0.
        }while(cbuff[++i]!=0x00);
        */
        settings_storage.led_red = (unsigned int)cbuff[3];
        settings_storage.led_green = (unsigned int)cbuff[4];
        settings_storage.led_blue = (unsigned int)cbuff[5];

        settings_save_config();
        settings_load_config();
        
        rgbled_set(settings_storage.led_red, settings_storage.led_green, settings_storage.led_blue);
        
        ble_write_bytes("[ok]",4);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    if(cbuff[0]=='\\' && cbuff[1]=='g' && cbuff[2]=='q')
    {
        recognized = 1;
        ble_write_bytes("[gq",3);
        ble_do_events();
        char buffer[3];
        
        sprintf(buffer,"%c%c%c", settings_storage.led_red,  settings_storage.led_green, settings_storage.led_blue);
        ble_write_bytes(buffer,3);
        ble_do_events();
        
        ble_write_bytes("]",1);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
        inicbuff();
    }
    
    inicbuff();
}

void commandEvent(void) 
{
    if(ble_connected())
    {
        
        while(ble_connected())
        {
            ble_do_events();
            
            //rgbled_set(0, 0, settings_storage.led_brightness); // turn blue led on
            rgbled_set(settings_storage.led_red, settings_storage.led_green, settings_storage.led_blue);
            while(ble_available())
            {
                rcvchar=0x00;
                rcvchar=(char)ble_read();
                addcbuff(rcvchar);
                ble_do_events();
            }
            if(flagcommand) execute_command();
        }
    }
    else
    {
        ble_do_events();
    }
}


