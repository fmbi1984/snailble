/*
 * usb.c
 *
 * Created: 23/02/2014 05:09:18
 *  Author: VendWatch
 */ 

#include "usb.h"

#include "board.h"
#include "wiring.h"
#include "wiring_digital.h"

void usb_begin(void)
{
  pinMode(PIN_USB_VBUS, INPUT);
}

bool usb_attached(void)
{
  pinMode(PIN_USB_VBUS, INPUT);
  return digitalRead(PIN_USB_VBUS)?false:true;
}