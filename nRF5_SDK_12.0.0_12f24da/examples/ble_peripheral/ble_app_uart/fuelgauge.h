#ifndef FUELGAUGE_H_
#define FUELGAUGE_H_

#include "wiring.h"

#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>

#define MAX17040_ADDRESS	0x36 //0x6C RoL

#define VCELL_REGISTER		0x02
#define SOC_REGISTER		0x04
#define MODE_REGISTER		0x06
#define VERSION_REGISTER	0x08
#define CONFIG_REGISTER		0x0C
#define COMMAND_REGISTER	0xFE

// When Connected to the USB
#define BATT_MIN_CHARGING	20
#define BATT_MAX_CHARGING	80

// When in Use
#define BATT_MIN_NORMAL		45
#define BATT_MAX_NORMAL		30

#define BATT_EMPTY		15

void writeRegister(byte address, byte MSB, byte LSB);
void readRegister(byte startAddress, byte *MSB, byte *LSB);
void fuelgauge_begin(void);
void fuelgauge_end(void);
void fuelgauge_reset(void);
void fuelgauge_quickStart(void);
float fuelgauge_getVCell(void);
float fuelgauge_getSoC(void);
uint16_t fuelgauge_getVersion(void);
uint16_t fuelgauge_getRComp(void);


double showBatteryStateNormal(unsigned char brightness); // NORMAL MODE
double showBatteryStateCharging(unsigned char brightness); // USB Attatch


double showBatteryLevelStored(unsigned char brightness);

#endif /* FUELGAUGE_H_ */