#include "rgbled.h"

#include "config.h"
#include "sdk_config.h"

#include "app_pwm.h"

#include "board.h"
#include "settings.h"
#include "wiring.h"
#include "wiring_digital.h"

#include "WMath.h"

#include "pwm.h"

volatile double rgbMillis = 10500.0; /* Variable to store millisecond ticks */
volatile double valBrightness = 0;

volatile double rBrightness = 0;
volatile double gBrightness = 0;
volatile double bBrightness = 0;

volatile double redBrightness = 0;
volatile double greenBrightness = 0;
volatile double blueBrightness = 0;

int pwm_max = 4095;
bool rgbled_ready = false;
static int led_toggle_var = 0;

APP_PWM_INSTANCE(PWM2,2);                   // Create the instance "PWM2" using TIMER2.

void rgbled_begin(void)
{
  
  //ret_code_t err_code;
  
  if(rgbled_ready == false) 
  {
      // Initialize the PWM library
      nrf_pwm_config_t pwm_config = PWM_DEFAULT_CONFIG;
      pwm_config.mode             = PWM_MODE_LED_4095;
      pwm_config.num_channels     = 3;
      pwm_config.gpio_num[0]      = 17;
      pwm_config.gpio_num[1]      = 18;
      pwm_config.gpio_num[2]      = 19;

      // Initialize and enable PWM.
      nrf_pwm_init(&pwm_config);

      rgbled_ready = true;

      led_toggle_var = 0;
      rgbMillis = 10500.0;
  }
}

void rgbled_setRAW(uint32_t iRed, uint32_t iGreen, uint32_t iBlue)
{
    if(rgbled_ready == true) 
    {
        nrf_pwm_set_value(0, iRed);
        nrf_pwm_set_value(1, iGreen);
        nrf_pwm_set_value(2, iBlue);
        
    }
}

void rgbled_set(uint32_t iRed, uint32_t iGreen, uint32_t iBlue)
{
    if(rgbled_ready == true) 
    {
        nrf_pwm_set_value(0, map(iRed, 0, 255, 0, 4095));
        nrf_pwm_set_value(1, map(iGreen, 0, 255, 0, 4095));
        nrf_pwm_set_value(2, map(iBlue, 0, 255, 0, 4095));
    }
}


void rgbled_end(void)
{
  if(rgbled_ready == true) 
  {
   
    nrf_pwm_set_value(0, 0);
    nrf_pwm_set_value(1, 0);
    nrf_pwm_set_value(2, 0);

    app_pwm_disable(&PWM2);
    
    app_pwm_uninit(&PWM2);
    rgbled_ready = false;
  }
}

void led_toggle()
{
   if(led_toggle_var == 0)
        led_toggle_var = 1;
    else
        led_toggle_var = 0;
    
    if(led_toggle_var==0)
    {
        rgbled_set(0, 0, 10);
    }
    else
    {
        rgbled_set(0, 0, settings_storage.led_brightness);
    }
}

bool rgbled_isReady()
{
    return rgbled_ready;
}
