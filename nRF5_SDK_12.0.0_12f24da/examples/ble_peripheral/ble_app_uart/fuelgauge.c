#include "fuelgauge.h"

#include "config.h"
#include "sdk_config.h"

#include "wiring.h"
#include "wiring_digital.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>


#include "app_uart.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"
#include "app_error.h"
#include "nrf.h"
#include "bsp.h"
#include "app_util_platform.h"
#include "nrf_delay.h"
#include "rgbled.h"


static const nrf_drv_twi_t m_twi_master = NRF_DRV_TWI_INSTANCE(MASTER_TWI_INST);
static unsigned char i2c_address;

////////////////////////////////////////////////////////////////////////////////
static void i2c_begin(void)
{
    ret_code_t ret;
    const nrf_drv_twi_config_t config =
    {
       .scl                = TWI_SCL_M,
       .sda                = TWI_SDA_M,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };
    
    nrf_drv_twi_uninit(&m_twi_master);

    do
    {
        ret = nrf_drv_twi_init(&m_twi_master, &config, NULL, NULL);
        if(NRF_SUCCESS != ret)
        {
            break;
        }
        nrf_drv_twi_enable(&m_twi_master);
    }while(0);
    //return ret;
}

static void i2c_end()
{
  nrf_drv_twi_disable(&m_twi_master);
  nrf_drv_twi_uninit(&m_twi_master);
}

static unsigned char i2c_start(unsigned char address)
{
  i2c_address = address;
  return 0;
}


static long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

////////////////////////////////////////////////////////////////////////////////
void writeRegister(byte address, byte MSB, byte LSB)
{
  /*
  i2c_start((MAX17040_ADDRESS << 1) | I2C_WRITE);
  i2c_write(address);
  i2c_write( MSB);
  i2c_write( LSB);
  i2c_stop();
  */
  
  i2c_address = MAX17040_ADDRESS;
  //uint32_t err_code;
  uint8_t tx_data[] = {address, MSB, LSB};
  uint8_t len =  sizeof(tx_data);
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, len, false);
  
  //delay(1);
}

void readRegister(byte startAddress, byte *MSB, byte *LSB)
{
  /*
  i2c_start((MAX17040_ADDRESS << 1) | I2C_WRITE);
  i2c_write( startAddress);
  
  i2c_start((MAX17040_ADDRESS << 1) | I2C_READ);
  
  RMSB = i2c_readAck();
  RLSB = i2c_readNack();
  //i2c_stop();
  */
  
  i2c_address = MAX17040_ADDRESS;
  //uint32_t err_code;
  uint8_t tx_data[] = {startAddress};
  uint8_t tx_len =  sizeof(tx_data);
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, true);

  uint8_t rx_data[2];
  uint8_t rx_len =  sizeof(rx_data);
  nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len);
  *MSB = rx_data[0];
  *LSB = rx_data[1];
}

void fuelgauge_begin(void)
{
  i2c_begin();
}

void fuelgauge_end(void)
{
  i2c_end();
  /*
  pinMode(TWI_SCL_M, OUTPUT);
  pinMode(TWI_SDA_M, OUTPUT);
  digitalWrite(TWI_SCL_M, LOW);
  digitalWrite(TWI_SDA_M, LOW);
  */
}

void fuelgauge_reset(void)
{
  writeRegister(COMMAND_REGISTER, 0x00, 0x54);
}

void fuelgauge_quickStart(void)
{
  writeRegister(MODE_REGISTER, 0x40, 0x00);	
}

float fuelgauge_getVCell(void)
{
  byte MSB = 0;
  byte LSB = 0;

  readRegister(VCELL_REGISTER, &MSB, &LSB);
  int value = (MSB << 4) | (LSB >> 4);
  return map(value, 0x000, 0xFFF, 0, 50000) / 10000.0;
  // return value * 0.00125;
}

float fuelgauge_getSoC()
{
  byte MSB = 0;
  byte LSB = 0;

  readRegister(SOC_REGISTER, &MSB, &LSB);
  float decimal = LSB / 256.0;
  
  if( (MSB+decimal) >= 100.0)
      return 100.0;
  else
      return (MSB + decimal);
}

uint16_t fuelgauge_getVersion()
{
  byte MSB = 0;
  byte LSB = 0;

  readRegister(VERSION_REGISTER, &MSB, &LSB);
  uint16_t version =  (MSB<<8) | LSB;
  return version;
}

uint16_t fuelgauge_getRComp()
{
  byte MSB = 0;
  byte LSB = 0;

  readRegister(CONFIG_REGISTER, &MSB, &LSB);
  uint16_t config =  (MSB<<8) | LSB;
  return config;
}

double getBatteryState(int show)
{
    double charge = (double)fuelgauge_getSoC();
    if(charge>100.0f)charge=100.0f;
    if(show)
    {
        if(charge<BATT_MIN_CHARGING)
        {
            rgbled_set(255, 0, 0);
        }

        if(charge>=BATT_MIN_CHARGING)
        {
            rgbled_set(0, 255, 0);
        }
    }
    return charge;
}

double showBatteryStateNormal(unsigned char brightness) // NORMAL MODE
{
    double charge = 0.0f;
    do
    {
        charge = (double)fuelgauge_getSoC();
    }while(charge <= 0.0f);

    if(charge>100.0f)charge=100.0f;
    
    settings_storage.battery_level = charge;
    settings_save_config();
    settings_load_config();

    if(charge<BATT_MIN_NORMAL)
    {
        rgbled_set(brightness, 0, 0);
    }

    if(charge>=BATT_MIN_NORMAL)
    {
        rgbled_set(0, brightness, 0);
    }
    
    return charge;
}

double showBatteryStateCharging(unsigned char brightness) // USB Attatch
{
    do{}while(fuelgauge_getSoC()<=0.0f);
    double charge = (double)fuelgauge_getSoC();
    if(charge>100.0f)charge=100.0f;
    settings_storage.battery_level = charge;
    settings_save_config(); //No
    settings_load_config(); //No

    if(charge<BATT_MIN_CHARGING)
    {
        //rgbled_breathingStart(brightness, 0, 0);
    }

    if(charge>=BATT_MIN_CHARGING && charge<BATT_MAX_CHARGING)
    {
        //rgbled_breathingStart(brightness/2, brightness/2, 0);
    }

    if(charge>=BATT_MAX_CHARGING)
    {
        //rgbled_breathingStart(0, brightness, 0);
    }
    return charge;
}

double showBatteryLevelStored(unsigned char brightness)
{
    if(settings_storage.battery_level<BATT_MIN_NORMAL)
    {
        rgbled_set(brightness, 0, 0);
    }

    if(settings_storage.battery_level>=BATT_MIN_NORMAL)
    {
        rgbled_set(0, brightness, 0);
    }
    return settings_storage.battery_level;
}