/*
 * settings.c
 *
 * Created: 20/02/2014 10:30:09
 *  Author: Francisco Betancourt
 */ 

#include "settings.h"
#include "eeprom.h"
#include <stdint.h>

void settings_load_config(void) 
{
  uint8_t data = 0;
  unsigned int addr = 0;
  eeprom_begin();
  for (unsigned int t=0; t<sizeof(settings_storage); t++)
  {
          addr = CONFIG_START + t;
          data = eeprom_read(addr);
          *((char*)&settings_storage + t) = data;		
  }
}

void settings_save_config(void) 
{
  uint8_t data = 0;
  unsigned int addr = 0;
  eeprom_begin();
  for (unsigned int t=0; t<sizeof(settings_storage); t++)
  {
          addr = CONFIG_START + t;
          data =  *((char*)&settings_storage + t);
          eeprom_write(addr, data);		
  }
}