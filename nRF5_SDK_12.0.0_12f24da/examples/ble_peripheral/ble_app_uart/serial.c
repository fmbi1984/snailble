#include "serial.h"

#include "config.h"
#include "sdk_config.h"

#include <stdarg.h>

//#include "app_uart.h"
#include "app_fifo.h"

#include "config.h"
#include "board.h"
#include "wiring.h"
#include "wiring_digital.h"

#include "sdk_common.h"
#include "nordic_common.h"

#include "rgbled.h"

app_fifo_t                  m_rx_fifo;                               /**< RX FIFO buffer for storing data received on the UART until the application fetches them using app_uart_get(). */
app_fifo_t                  m_tx_fifo;                               /**< TX FIFO buffer for storing data to be transmitted on the UART when TXD is ready. Data is put to the buffer on using app_uart_put(). */

unsigned long _startMillis;  // used for timeout measurement
unsigned long _timeout;      // number of milliseconds to wait for the next char before aborting timed read

volatile bool serial_ready = false;

static void uart_error_handle(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}

void serial_begin()
{
  _timeout = 25;
  
  ret_code_t err_code;
  //pinMode(SERIALPORT_PIN_RX, HIGH_Z);
  const app_uart_comm_params_t comm_params =
    {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud9600
    };
  
  if(serial_ready == false)
  {
      APP_UART_FIFO_INIT(&comm_params,
                           UART_RX_BUF_SIZE,
                           UART_TX_BUF_SIZE,
                           uart_error_handle,
                           APP_IRQ_PRIORITY_LOW,
                           err_code);
      
      
      APP_ERROR_CHECK(err_code);
      serial_ready = true;
  }
  
}

bool serial_attached(void)
{
  pinMode(SERIALPORT_PIN_MAX_VALID, INPUT);
  return digitalRead(SERIALPORT_PIN_MAX_VALID)?false:true;
}

void serial_sleep(void)
{
  pinMode(SERIALPORT_PIN_FORCE_ON, OUTPUT); // set FORCE ON pin as output
  digitalWrite(SERIALPORT_PIN_FORCE_ON, LOW); // set FORCE ON pin lo
  
  pinMode(SERIALPORT_PIN_FORCE_OFF, OUTPUT); // set FORCE OFF pin as output
  digitalWrite(SERIALPORT_PIN_FORCE_OFF, LOW); // set FORCE OFF pin lo 
}

void serial_wakeup(void)
{
  //app_uart_rx_disable();
  //delay(5);
  pinMode(SERIALPORT_PIN_MAX_VALID, INPUT);

  pinMode(SERIALPORT_PIN_FORCE_ON, OUTPUT); // set FORCE ON pin as output
  digitalWrite(SERIALPORT_PIN_FORCE_ON, HIGH); // set FORCE ON pin hi
  
  pinMode(SERIALPORT_PIN_FORCE_OFF, OUTPUT); // set FORCE OFF pin as output
  digitalWrite(SERIALPORT_PIN_FORCE_OFF, HIGH); // set FORCE OFF pin hi
  //delay(5);
  //app_uart_rx_enable();
}

/*
uint8_t serial_write(uint8_t c)
{
  printf("%c", c);
  return 1;
}*/

int serial_writeBytes(char *buffer, int size)
{
  int n = 0;
  while (size--) {
    n += serial_write(*buffer++);
  }
  return n;
}

uint8_t serial_writeString(char* data)
{
  uint8_t result = 0;
  // While not end of transmit string
  while(*data != '\0')
  {
    // Print string one char at a time
    serial_write(*data++);// Write and increment char position in string
  }
  result = 1;
  return result;
}

int serial_print(char* str)
{
  return serial_writeBytes(str, sizeof(str)*8);
}

void serial_printf(char *fmt, ... )
{
  char tmp[128]; // resulting string limited to 128 chars
  va_list args;
  va_start (args, fmt );
  vsnprintf(tmp, 128, fmt, args);
  va_end (args);
  serial_writeString(tmp);
}

int serial_readBytes(char *buffer, int length)
{
	size_t count = 0;
	while (count < length) {
		int c = serial_timedRead();
		if (c < 0) break;
		*buffer++ = (char)c;
		count++;
	}
	return count;
}

int serial_timedRead()
{
	int c;
	_startMillis = millis();
	do {
		c = serial_read();
		if (c >= 0) return c;
	} while(millis() - _startMillis < _timeout);
	return -1;     // -1 indicates timeout
}

void serial_setTimeout(unsigned long timeout)  // sets the maximum number of milliseconds to wait
{
	_timeout = timeout;
}

void serial_flush()
{
  app_uart_flush();
  //uint32_t err_code;

  ////err_code = app_fifo_flush(&m_tx_fifo);
  //err_code = app_fifo_flush(&m_rx_fifo);
  
  //(&m_rx_fifo)->read_pos = 0;
  //(&m_rx_fifo)->write_pos = 0;

  //VERIFY_SUCCESS(err_code);
}

void serial_status()
{
  serial_printf("\r\nread_pos=%d\r\n", (&m_rx_fifo)->read_pos);
  serial_printf("\r\nwrite_pos=%d\r\n", (&m_rx_fifo)->write_pos);
}

////////////////////////////////////////////////////////////////////////////////

int serial_available(void)
{
  //return (int)FIFO_LENGTH(m_rx_fifo);
  app_fifo_t * p_fifo = &m_rx_fifo;
  uint32_t r_tmp = p_fifo->read_pos;
  uint32_t w_tmp = p_fifo->write_pos;
  uint32_t l_tmp = w_tmp - r_tmp;
  return l_tmp;
}

int serial_read()
{
  uint8_t p_byte = 0;
  
  app_fifo_t * p_fifo = &m_rx_fifo;
  
  if((p_fifo->read_pos) == (p_fifo->write_pos)) {
    return -1;
  }
  else {
    app_fifo_get(&m_rx_fifo, &p_byte);
    //fifo_get(&m_rx_fifo, &p_byte);
    led_toggle();
  }
  
  return (int)p_byte;
}

int serial_write(uint8_t c)
{
  while(app_uart_put(c) != NRF_SUCCESS);
  led_toggle();
  return 1;
}

void serial_end()
{
    if(serial_ready == true)
    {
        app_uart_flush();
        app_uart_close();
        serial_ready = false;
    }
    serial_sleep();
    //pinMode(SERIALPORT_PIN_RX, HIGH_Z);
}