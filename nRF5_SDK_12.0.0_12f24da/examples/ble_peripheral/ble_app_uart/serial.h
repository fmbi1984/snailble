#ifndef SERIAL_H_
#define SERIAL_H_

#include "wiring.h"

void serial_begin();
void serial_end();
bool serial_attached(void);
void serial_sleep(void);
void serial_wakeup(void);

int serial_writeBytes(char *buffer, int size);
uint8_t serial_writeString(char* data);
int serial_print(char* str);
void serial_printf(char *fmt, ... );

void serial_flush();

int serial_readBytes(char *buffer, int length);
int serial_timedRead();
void serial_setTimeout(unsigned long timeout);

////////////////////////////////////////////////////////////////////////////////

int serial_available(void);
int serial_read();
int serial_write(uint8_t c);

#endif /* SERIAL_H_ */