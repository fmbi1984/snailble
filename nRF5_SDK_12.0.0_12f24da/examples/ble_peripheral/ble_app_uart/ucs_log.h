/*
 * ucs_log.h
 *
 * Created: 24/02/2014 21:56:54
 *  Author: VendWatch
 */ 

#ifndef UCS_LOG_H_
#define UCS_LOG_H_

#define LOG_ERROR 0
#define LOG_INFO 1
#define LOG_DEBUG 2
#define LOG_DEXLOG 4

void logg(int level, const char *format, ...);

#endif /* UCS_LOG_H_ */