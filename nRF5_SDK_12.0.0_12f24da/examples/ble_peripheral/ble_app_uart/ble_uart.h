#ifndef BLE_UART_H_
#define BLE_UART_H_

#include "wiring.h"

void power_manage(void);

void ble_begin(char* device_name, char* serial_number);
void ble_end(void);
int ble_read(void);
void ble_write(unsigned char data);
void ble_write_bytes(char *data, uint8_t len);

unsigned char ble_available(void);
unsigned char ble_connected(void);
void ble_do_events(void);
void ble_disconnect();

#endif /* BLE_UART_H_ */