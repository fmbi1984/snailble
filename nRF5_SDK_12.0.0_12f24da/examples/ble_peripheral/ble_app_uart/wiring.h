#ifndef WIRING_H_
#define WIRING_H_


#include "config.h"
#include "sdk_config.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>


#include "ble_gap.h"
#include "app_uart.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"
#include "app_error.h"
#include "nrf.h"
//#include "bsp.h"
#include "app_util_platform.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_timer.h"
#include "nrf_soc.h"
#include "softdevice_handler.h"

#include "settings.h"
typedef uint8_t byte;

//#include "WMath.h"

void init(void);
void delay(unsigned long ms);
void delayMicroseconds(unsigned int us);
unsigned long millis(void);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool stopwatch_isRunning(void);
void stopwatch_begin(void);
void stopwatch_stop(void);
void stopwatch_start(void);
void stopwatch_reset(void);

uint32_t stopwatch_elapsed(void);
void stopwatch_setElapsed(long elapsed);
void stopwatch_overflow();

void timer_wiring_stop();
void timer_wiring_start();

int get_wiring_flag();
void set_wiring_flag();
void clear_wiring_flag();

#endif /* WIRING_H_ */