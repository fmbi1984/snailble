#include "accelerometer.h"

#include "config.h"
#include "sdk_config.h"

#include <stdbool.h>
#include <stdint.h>

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "app_uart.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"
#include "app_error.h"
#include "nrf.h"
#include "bsp.h"
#include "app_util_platform.h"
#include "nrf_delay.h"
#include "board.h"
#include "wiring.h"
#include "wiring_digital.h"

static const nrf_drv_twi_t m_twi_master = NRF_DRV_TWI_INSTANCE(MASTER_TWI_INST);
static unsigned char i2c_address;

////////////////////////////////////////////////////////////////////////////////
static void i2c_begin(void)
{
    ret_code_t ret;
    const nrf_drv_twi_config_t config =
    {
       .scl                = TWI_SCL_M,
       .sda                = TWI_SDA_M,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };
    
    nrf_drv_twi_uninit(&m_twi_master);
    
    do
    {
        ret = nrf_drv_twi_init(&m_twi_master, &config, NULL, NULL);
        if(NRF_SUCCESS != ret)
        {
            break;
        }
        nrf_drv_twi_enable(&m_twi_master);
    }while(0);
    //return ret;
}

static void i2c_end()
{
  nrf_drv_twi_disable(&m_twi_master);
  nrf_drv_twi_uninit(&m_twi_master);
}

static unsigned char i2c_start(unsigned char address)
{
  i2c_address = address;
  return 0;
}

static void delay(unsigned long ms)
{
  nrf_delay_ms(ms);
}

static long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

////////////////////////////////////////////////////////////////////////////////

volatile bool ISRFlag;

bool motion_;
bool shake_;
bool shakeAxisX_;
bool shakeAxisY_;
bool shakeAxisZ_;

void accelerometer_begin(void)
{
  pinMode(ACCELEROMETER_INT1, INPUT);
  
  i2c_begin();
}

bool accelerometer_shakeMode(int threshold, bool enableX, bool enableY, bool enableZ, bool enableINT2)
{
  byte statusCheck;
  bool error = false;

  pinMode(ACCELEROMETER_INT1, INPUT);
  uint8_t tx_data[8];
  uint8_t tx_len;
  uint8_t rx_data[8];
  uint8_t rx_len;

  i2c_begin();

  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_CTRL_REG1;
  tx_data[1] = 0x18;
  tx_len = 2;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, false); // stop transmitting & hang up
  delay(5);

  byte xyzCfg = 0x10; //latch always enabled
  if(enableX) xyzCfg |= 0x02;
  if(enableY) xyzCfg |= 0x04;
  if(enableZ) xyzCfg |= 0x08;

  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_TRANSIENT_CFG;
  tx_data[1] = xyzCfg;
  tx_len = 2;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, false); // stop transmitting & hang up
  delay(5);

  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_TRANSIENT_CFG;
  tx_len = 1;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, true); // don't hang up - repeated start

  rx_len = 1;
  nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len);
  statusCheck = rx_data[0];
  delay(5);

  if(statusCheck != xyzCfg)error = true;

  if(threshold > 127) threshold = 127; //8g is the max.

  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_TRANSIENT_THS;
  tx_data[1] = threshold;
  tx_len = 2;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, false); // stop transmitting & hang up

  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_TRANSIENT_THS;
  tx_data[1] = threshold;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, 2, true); // don't hang up - repeated start
        
  rx_len = 1;
  nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len); // stop transmitting - hang up
  statusCheck = rx_data[0];
  delay(5);

  if(statusCheck != threshold) error = true;
        
  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_TRANSIENT_COUNT;
  tx_data[1] = 0x05;
  tx_len = 2;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, false); // stop transmitting & hang up
  delay(5);
  
  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_TRANSIENT_COUNT;
  tx_len = 1;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, true);  // don't hang up - repeated start
  
  rx_len = 1;
  nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len); // stop transmitting - hang up
  statusCheck = rx_data[0];
  delay(5);
        
  if(statusCheck != 0x05) error = true;
        
  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_CTRL_REG4;
  tx_len = 1;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, true);  // don't hang up - repeated start
  
  rx_len = 1;
  nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len); // stop transmitting - hang up
  statusCheck = rx_data[0];
  delay(5);

  statusCheck |= 0x20;
        
  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_CTRL_REG4;
  tx_data[1] = statusCheck;
  tx_len = 2;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, false); // stop transmitting & hang up
  delay(5);

  byte intSelect = 0x20;

  if(enableINT2) intSelect = 0x00;
        
  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_CTRL_REG5;
  tx_len = 1;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, true);  // don't hang up - repeated start
  
  rx_len = 1;
  nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len); // stop transmitting - hang up
  statusCheck = rx_data[0];
  delay(5);

  statusCheck |= intSelect;

  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_CTRL_REG5;
  tx_data[1] = statusCheck;
  tx_len = 2;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, false); // stop transmitting & hang up
  delay(5);

  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_CTRL_REG1;
  tx_len = 1;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, true);  // don't hang up - repeated start
  
  rx_len = 1;
  nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len); // stop transmitting - hang up
  statusCheck = rx_data[0];
  delay(5);

  statusCheck |= 0x01; //Change the value in the register to Active Mode.
        
  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_CTRL_REG1;
  tx_data[1] = statusCheck;
  tx_len = 2;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, false); // stop transmitting & hang up
  delay(5);
  
  return error;

}

void accelerometer_clearInterrupt()
{
  uint8_t tx_data[8];
  uint8_t tx_len;
  uint8_t rx_data[8];
  uint8_t rx_len;
  
  byte sourceSystem;
  //i2c_begin();
  //////////////////////////////////////////////////////////////////////////////
  
  i2c_address = MMA8451Q_ADDRESS;
  tx_data[0] = MMA8451Q_REG_INT_SOURCE;
  tx_len = 1;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, true);  // don't hang up - repeated start
  
  rx_len = 1;
  nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len); // stop transmitting - hang up
  sourceSystem = rx_data[0];
  delay(5);
  //////////////////////////////////////////////////////////////////////////////
  
  shake_ = false;
  motion_= false;
  shakeAxisX_ = false;
  shakeAxisY_ = false;
  shakeAxisZ_ = false;
  
  if((sourceSystem&0x20) == 0x20) //Transient
  {	
    //Perform an Action since Transient Flag has been set
    //Read the Transient to clear system interrupt and Transient
    byte srcTrans;
    shake_ = true;
    ////////////////////////////////////////////////////////////////////////////
    i2c_address = MMA8451Q_ADDRESS;
    tx_data[0] = MMA8451Q_REG_TRANSIENT_SRC;
    tx_len = 1;
    nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, true);  // don't hang up - repeated start
    
    rx_len = 1;
    nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len); // stop transmitting - hang up
    srcTrans = rx_data[0];
    delay(5);
    ////////////////////////////////////////////////////////////////////////////
    if((srcTrans&0x02) == 0x02)
    {
          shakeAxisX_ = true;
    }
    if((srcTrans&0x08) == 0x08)
    {
          shakeAxisY_ = true;
    }
    if((srcTrans&0x20) == 0x20)
    {
          shakeAxisZ_ = true;
    }
  }

  if((sourceSystem&0x04) == 0x04) //FreeFall Motion
  {
    ////////////////////////////////////////////////////////////////////////////
    i2c_address = MMA8451Q_ADDRESS;
    tx_data[0] = MMA8451Q_REG_FF_MT_SRC;
    tx_len = 1;
    nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, true);  // don't hang up - repeated start
    
    rx_len = 1;
    nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len); // stop transmitting - hang up
    rx_data[0];
    delay(5);
    ///////////////////////////////////////////////////////////////////////////////////////////////
    motion_ = true;
  }
  
}

bool accelerometer_shaked(void)
{
    pinMode(ACCELEROMETER_INT1, INPUT);
    return digitalRead(ACCELEROMETER_INT1)?false:true;
}

