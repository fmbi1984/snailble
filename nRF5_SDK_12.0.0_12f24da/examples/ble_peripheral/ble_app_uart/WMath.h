/*
 * WMath.h
 *
 * Created: 20/02/2014 21:04:50
 *  Author: VendWatch
 */ 

#ifndef WMATH_H_
#define WMATH_H_

#include <stdlib.h>
#include <math.h>

// undefine stdlib's abs if encountered
#ifdef abs
#undef abs
#endif

// undefine math's round if encountered
#ifdef round
#undef round
#endif

#ifdef PI
#undef PI 
#endif

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966192313216916398
#define TWO_PI 6.283185307179586476925286766559
#define DEG_TO_RAD 0.017453292519943295769236907684886
#define RAD_TO_DEG 57.295779513082320876798154814105

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define abs(x) ((x)>0?(x):-(x))
#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))
#define round(x)     ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))
#define radians(deg) ((deg)*DEG_TO_RAD)
#define degrees(rad) ((rad)*RAD_TO_DEG)
#define sq(x) ((x)*(x))

void randomSeed(unsigned int seed);
long random_max(long howbig);
long random_min_max(long howsmall, long howbig);

long map(long x, long in_min, long in_max, long out_min, long out_max);

unsigned int makeWord_Word(unsigned int w);
unsigned int makeWord_highByte_lowByte(unsigned char h, unsigned char l);

#endif /* WMATH_H_ */