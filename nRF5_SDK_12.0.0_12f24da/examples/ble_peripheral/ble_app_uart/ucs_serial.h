/*
 * ucs_serial.h
 *
 * Created: 24/02/2014 21:57:34
 *  Author: VendWatch
 */ 


#ifndef UCS_SERIAL_H_
#define UCS_SERIAL_H_

/*
** This is the DEX Port  - usually a UART number for a uproc
*/
#define DEX_PORT 0

int uts(char *buffer, int count);
int utr(char *buffer, int count);

int serialSendReceive(char *outbuf, int outcount,char *inbuf, int incount,int exact, int timeout);
int serialReadChar(char *charaddr, int tWait);

int numCharsWaiting(int PORT);
int txCharsWaiting(int PORT);
void clearSerialPort(int PORT);

#endif /* UCS_SERIAL_H_ */