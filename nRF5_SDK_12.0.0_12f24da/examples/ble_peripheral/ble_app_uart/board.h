#ifndef BOARD_H_
#define BOARD_H_

#define ACCELEROMETER_INT1              13

#define SERIALPORT_PIN_FORCE_ON         11
#define SERIALPORT_PIN_FORCE_OFF        12
#define SERIALPORT_PIN_MAX_VALID        14
#define SERIALPORT_PIN_RX               8


#define PIN_USB_VBUS                    15

#define PIN_SCL                         3
#define PIN_SDA                         4
#define PIN_BATT_CHARGED                16

#define RED_LED                         17
#define GREEN_LED                       18
#define BLUE_LED                        19

#endif /* BOARD_H_ */