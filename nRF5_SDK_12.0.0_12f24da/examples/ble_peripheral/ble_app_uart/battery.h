/*
 * battery.h
 *
 * Created: 19/02/2014 00:31:51
 *  Author: VendWatch
 */ 


#ifndef BATTERY_H_
#define BATTERY_H_

//#include <stdio.h>
#include <stdbool.h>

void battery_begin(void);
bool battery_charging(void);
bool battery_fullycharged(void);


#endif /* BATTERY_H_ */