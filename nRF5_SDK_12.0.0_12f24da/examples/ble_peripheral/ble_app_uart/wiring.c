#include "wiring.h"

#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"
#include "nrf_drv_timer.h"
#include "bsp.h"
#include "app_error.h"

struct StoreStruct settings_storage;

const nrf_drv_timer_t timer0 = NRF_DRV_TIMER_INSTANCE(3);

volatile uint32_t msElapsed = 0;
volatile bool isRunning = false;
volatile bool wiring_ready = false;
volatile int wiring_flag = 0;

volatile uint32_t msTicks = 0; /* Variable to store millisecond ticks */

/* TimerTick interrupt Handler. */
void timer_event_handler(nrf_timer_event_t event_type, void* p_context)
{
  switch(event_type)
  {
      case NRF_TIMER_EVENT_COMPARE0:

          //__disable_irq();
          msTicks+=1;
          if(isRunning)
          {
            msElapsed+=1;
          }
          set_wiring_flag();
          //__enable_irq();
          //digitalToggle(17);
          break;

      default:
          //Do nothing.
          break;
  }

}

void init(void)
{
    /*
    uint32_t time_ms = 1; //Time(in miliseconds) between consecutive compare events.
    uint32_t time_ticks;
    uint32_t err_code = NRF_SUCCESS;

    //pinMode(17, OUTPUT);

    err_code = nrf_drv_timer_init(&timer0, NULL, timer_event_handler);
    //SEGGER_RTT_printf(0,"nrf_drv_timer_init\r\n");
    APP_ERROR_CHECK(err_code);

    time_ticks = nrf_drv_timer_ms_to_ticks(&timer0, time_ms);
    nrf_drv_timer_extended_compare(&timer0, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);

    nrf_drv_timer_enable(&timer0);
    */
    
    if (((NRF_UICR->PSELRESET[0] & UICR_PSELRESET_CONNECT_Msk) != (UICR_PSELRESET_CONNECT_Connected << UICR_PSELRESET_CONNECT_Pos)) || 
        ((NRF_UICR->PSELRESET[0] & UICR_PSELRESET_CONNECT_Msk) != (UICR_PSELRESET_CONNECT_Connected << UICR_PSELRESET_CONNECT_Pos))){
        NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
        while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
        NRF_UICR->PSELRESET[0] = 21;
        while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
        NRF_UICR->PSELRESET[1] = 21;
        while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
        NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
        while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
        NVIC_SystemReset();
    }

    timer_wiring_start();
}

/*
void delay(unsigned long ms)
{
  nrf_delay_ms(ms);
}
*/

void delay(unsigned long ms)
{
    if(wiring_ready)
    {
        unsigned long start = millis();
        while ((millis() - start) <= ms);
    }
}

void delayMicroseconds(unsigned int us)
{
    if(wiring_ready)
    {
      nrf_delay_us(us);
    }
}

unsigned long millis(void)
{
    unsigned long m = 0;
    if(wiring_ready)
    {
      //uint8_t oldSREG = SREG;

      // disable interrupts while we read rtc_millis or we might get an
      // inconsistent value (e.g. in the middle of a write to rtc_millis)

      //cli();
      //__disable_irq();
      m = msTicks;
      //__enable_irq();
      //SREG = oldSREG;
      //sei();
    }
    return m;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void stopwatch_begin(void)
{
    isRunning = false;
    msElapsed = 0;
}

void stopwatch_stop(void)
{
    isRunning = false;
}

void stopwatch_start(void)
{
    isRunning = true;
}

void stopwatch_reset(void)
{
    msElapsed = 0;
}

void stopwatch_setElapsed(long elapsed)
{
    msElapsed = elapsed;
}

void stopwatch_overflow()
{
    msElapsed = 1000000L;
}


uint32_t stopwatch_elapsed(void)
{
    uint32_t time = msElapsed;
    return time;
}

void timer_wiring_stop()
{
    if(wiring_ready == true)
    {
        nrf_drv_timer_disable(&timer0);
        nrf_drv_timer_uninit(&timer0);
        wiring_ready = false;
    }
}

void timer_wiring_start()
{
    if(wiring_ready == false)
    {
        uint32_t time_ms = 1; //Time(in miliseconds) between consecutive compare events.
    uint32_t time_ticks;
    uint32_t err_code = NRF_SUCCESS;
    
    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
    err_code = nrf_drv_timer_init(&timer0, &timer_cfg, timer_event_handler);
    APP_ERROR_CHECK(err_code);

    time_ticks = nrf_drv_timer_ms_to_ticks(&timer0, time_ms);

    nrf_drv_timer_extended_compare(
         &timer0, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
        nrf_drv_timer_enable(&timer0);
        
        wiring_ready = true;
    }
}

int get_wiring_flag()
{
    return wiring_flag;
}

void set_wiring_flag()
{
    wiring_flag = 1;
}

void clear_wiring_flag()
{
    wiring_flag = 0;
}