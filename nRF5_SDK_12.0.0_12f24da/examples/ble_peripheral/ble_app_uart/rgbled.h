#ifndef RGBLED_H_
#define RGBLED_H_

#include "wiring.h"

void rgbled_setRAW(uint32_t iRed, uint32_t iGreen, uint32_t iBlue);
void rgbled_set(uint32_t iRed, uint32_t iGreen, uint32_t iBlue);
void rgbled_begin(void);
void rgbled_end(void);
void led_toggle();
bool rgbled_isReady();
#endif /* RGBLED_H_ */