#ifndef WIRING_DIGITAL_H_
#define WIRING_DIGITAL_H_

#include "wiring.h"

#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>

#define HIGH 0x1
#define LOW  0x0
#define HIGH_Z 0x2

#define INPUT 0x0
#define OUTPUT 0x1
#define INPUT_PULLUP 0x2
#define INPUT_PULLDOWN 0x3

void pinMode(uint8_t pin, uint8_t mode);
void digitalWrite(uint8_t pin, uint8_t val);
void digitalToggle(uint8_t pin);
int digitalRead(uint8_t pin);

#endif /* WIRING_DIGITAL_H_ */
