/*
 * battery.c
 *
 * Created: 19/02/2014 00:31:41
 *  Author: Francisco Betancourt
 */ 
#include "battery.h"

#include "board.h"
#include "wiring.h"
#include "wiring_digital.h"

void battery_begin(void)
{
  pinMode(PIN_BATT_CHARGED, INPUT);
}

bool battery_charging(void)
{
  return digitalRead(PIN_BATT_CHARGED)?false:true;
}

bool battery_fullycharged(void)
{
  return digitalRead(PIN_BATT_CHARGED)?true:false;
}