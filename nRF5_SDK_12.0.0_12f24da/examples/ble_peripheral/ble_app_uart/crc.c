/*
 * crc.c
 *
 * Created: 24/02/2014 21:49:43
 *  Author: VendWatch
 */ 
#include "crc.h"
//#include "stopwatch.h"
//#include "ucs_log.h"

unsigned short calc_crc(unsigned char *sBuffer, int iBufferLen)
{
	//logg(LOG_INFO |  LOG_DEXLOG,"begin crc");
	
	//stopwatch_begin();
	//stopwatch_stop();
	//stopwatch_reset();
	//stopwatch_start();
	
	unsigned short c=0;
	int j;
	unsigned short         bit_count;
	unsigned short         x16;
	unsigned short         carry;
	unsigned char value;
	for(j=0; j<iBufferLen; j++)
	{
		value=sBuffer[j];
		for( bit_count=0; bit_count<8; ++bit_count )
		{
			x16 = (value & 1) ^ (c & 1);
			value = value >> 1;
			if( x16 == 1 )
			{
				c = (unsigned short)( c ^ 0x4002 );
				carry = 1;
			}
			else
			{
				carry = 0;
			}
			c = c >> 1;
			if( carry == 1)
			{
				c = c | 0x8000;
			}
			else
			{
				c = c & 0x7fff;
			}
		}
	}
	
	//stopwatch_stop();
	//logg(LOG_INFO |  LOG_DEXLOG,"end crc");
	//logg(LOG_INFO |  LOG_DEXLOG,"elapsed %u ms",stopwatch_elapsed());
	
	return c;
}

unsigned short calc_crc_with_feed(unsigned char *sBuffer, int iBufferLen, unsigned short usCrc)
{
	//logg(LOG_INFO |  LOG_DEXLOG,"begin crc");
	
	//stopwatch_begin();
	//stopwatch_stop();
	//stopwatch_reset();
	//stopwatch_start();
	
	unsigned short c=usCrc;
	int j;
	unsigned short         bit_count;
	unsigned short         x16;
	unsigned short         carry;
	unsigned char value;
	for(j=0; j<iBufferLen; j++)
	{
		value=sBuffer[j];
		for( bit_count=0; bit_count<8; ++bit_count )
		{
			x16 = (value & 1) ^ (c & 1);
			value = value >> 1;
			if( x16 == 1 )
			{
				c = (unsigned short)( c ^ 0x4002 );
				carry = 1;
			}
			else
			{
				carry = 0;
			}
			c = c >> 1;
			if( carry == 1)
			{
				c = c | 0x8000;
			}
			else
			{
				c = c & 0x7fff;
			}
		}
	}
	
	//stopwatch_stop();
	//logg(LOG_INFO |  LOG_DEXLOG,"end crc");
	//logg(LOG_INFO |  LOG_DEXLOG,"elapsed %u ms",stopwatch_elapsed());
	
	return c;
}