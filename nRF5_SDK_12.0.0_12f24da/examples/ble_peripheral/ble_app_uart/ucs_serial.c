/*
 * ucs_serial.c
 *
 * Created: 24/02/2014 21:57:20
 *  Author: VendWatch
 */ 
#include "ucs_serial.h"

#include <string.h>
#include <stdarg.h>

#include "ucs.h"

#include "serial.h"

int uts(char *buffer, int count)
{
	return serial_writeBytes(buffer, count);
}

int utr(char *buffer, int count)
{
	return serial_readBytes(buffer, count);
}

int readchar(char *buf)
{
	if(utr(buf,1) > 0) return GPRS_OK;
	else return GPRS_ERROR;
}

int serialReadChar(char *charaddr, int atWait)
{
	int	nResult = RX_TIMEOUT;
	TIMER timerChar;
	int	bDone = ucsFALSE;
	unsigned long tWait = atWait;
	if(atWait <= 0)
	{
		tWait=(600000);  // 10 minutes in ms
	}
	startTimer(&timerChar,tWait);
	while(bDone == ucsFALSE)
	{
		if(readchar(charaddr)==GPRS_OK)
		{
			bDone=ucsTRUE;
			nResult=GPRS_OK;
		}
		else if(timerExpired(&timerChar))
		{
			bDone=ucsTRUE;
		}
	}
	return nResult;
}


void serwaitXmit()
{
	while(txCharsWaiting(DEX_PORT)>0)
	{
		msec_delay(20);
	}
}

int serialSend(char *sSendBuffer,int nSendLength)
{
	int nResult=0;
	int n;
	int k;
	while (nSendLength > 0)
	{
		if(nSendLength > 1024)
		{
			k=1024;
		}
		else
		{
			k=nSendLength;
		}
		n = uts(sSendBuffer+nResult, k);
		nResult += n;
		nSendLength-=n;
		if(n<k)
		{
			msec_delay(20L);
		}
	}
	serwaitXmit();
	return nResult;
}

int serialSendReceive(
char*		sSendBuffer,
int		nSendLength,
char*		sReceiveBuffer,
int		nReceiveLength,
int		bReceiveExact,
int		atTimeOut)
{
	int	status = GPRS_OK;
	char thischar;
	int	bDone = ucsFALSE;
	int	nTargetIndex = 0;
	TIMER timerTimeOut;
	unsigned long tTimeOut = atTimeOut;
	logg(LOG_INFO,"serialSendReceive() sLen=%d, rLen=%d, exact=%d, timeout=%d\r\n",
	nSendLength,nReceiveLength,bReceiveExact,atTimeOut);
	if(sSendBuffer && (nSendLength > 0))
	{
		serialSend(sSendBuffer, nSendLength);
	}
	if(nReceiveLength > 0)
	{
		if(tTimeOut==0)
		{
			tTimeOut=(60000L);	// one minute timeout
		}
		startTimer(&timerTimeOut,tTimeOut);
		while (bDone == ucsFALSE)
		{
			if(readchar(&thischar) == GPRS_OK)
			{
				if (bReceiveExact)	// looking for an exact match
				{
					if(sReceiveBuffer[nTargetIndex] != thischar)
					{
						nTargetIndex = 0;
					}
					else
					{
						nTargetIndex++;
					}
				}
				else	// not looking for exact match
				{
					sReceiveBuffer[nTargetIndex++] = thischar;
				}
				if (nTargetIndex >= nReceiveLength)
				{
					bDone=ucsTRUE;
					break;
				}
			}
			else if (timerExpired(&timerTimeOut))
			{
				bDone=ucsTRUE;
				status = RX_TIMEOUT;
			}
		}
	}
	return status;
}


int numCharsWaiting(int PORT)
{
	int retval=0;
	retval = serial_available();
	return retval;
}

int txCharsWaiting(int PORT)
{
	int retval=0;
	return retval;
}

void clearSerialPort(int PORT)
{
	serial_flush();
	return;
}

