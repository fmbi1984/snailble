/*
 * sleep.c
 *
 * Created: 23/02/2014 05:14:55
 *  Author: VendWatch
 */ 

#include "sleepmgr.h"

#include "board.h"
#include "wiring.h"
#include "wiring_digital.h"

#include "rgbled.h"
#include "serial.h"
#include "ble_uart.h"
#include "command_processor.h"
#include "ucs.h"
#include "battery.h"
#include "fuelgauge.h"
#include "usb.h"
#include "accelerometer.h"

void in_pin_13_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action) // Accelerometer
{
  
} //button 1

void in_pin_14_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action) // Serial INVALID
{
  
}

void in_pin_15_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action) // USB plugged in
{
  
}

void sleepmgr_sleep()
{
    ret_code_t err_code;
    delay(250);
    // Sleep
    ble_end();
    //serial_end();
    //pinMode(PIN_BATT_CHARGED, INPUT);
    //fuelgauge_end();
    //pinMode(SERIALPORT_PIN_RX, INPUT);
    rgbled_end();
    timer_wiring_stop();
    
    // Configure Interrupts and Sleep CPU
    nrf_drv_gpiote_uninit();
    err_code = nrf_drv_gpiote_init();
    APP_ERROR_CHECK(err_code);

    // Accelerometer
    nrf_drv_gpiote_in_config_t in_config_13 = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
    in_config_13.pull = NRF_GPIO_PIN_NOPULL; //NRF_GPIO_PIN_PULLUP;
    err_code = nrf_drv_gpiote_in_init(13, &in_config_13, in_pin_13_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(13, true);

    // Serial INVALID
    nrf_drv_gpiote_in_config_t in_config_14 = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
    in_config_14.pull = NRF_GPIO_PIN_NOPULL;
    err_code = nrf_drv_gpiote_in_init(14, &in_config_14, in_pin_14_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(14, true);
    
    // USB plugged in
    nrf_drv_gpiote_in_config_t in_config_15 = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
    in_config_15.pull = NRF_GPIO_PIN_NOPULL;
    err_code = nrf_drv_gpiote_in_init(15, &in_config_15, in_pin_15_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(15, true);
    
    /*
    __SEV(); // Clear Event Register
    __WFE(); // Wait for event
    __WFE(); // Wait for event 
    */
    
    __SEV();__WFE();__WFE();
    //while(!serial_attached() && !usb_attached() && !accelerometer_shaked()){};

    // Wakeup
    timer_wiring_start();
    rgbled_begin();
    //fuelgauge_begin();
    //fuelgauge_reset();
    //fuelgauge_quickStart();
    //serial_begin();
    //pinMode(SERIALPORT_PIN_RX, HIGH_Z);
    //usb_begin();
}
