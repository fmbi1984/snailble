/*
 * WMath.c
 *
 * Created: 20/02/2014 21:05:12
 *  Author: Francisco Betancourt
 */ 

#include "WMath.h"

void randomSeed(unsigned int seed)
{
  if (seed != 0) {
    srand(seed);
  }
}

long random_max(long howbig)
{
  if (howbig == 0) {
    return 0;
  }
  return rand() % howbig;
}

long random_min_max(long howsmall, long howbig)
{
  if (howsmall >= howbig) {
    return howsmall;
  }
  long diff = howbig - howsmall;
  return random_max(diff) + howsmall;
}

long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

unsigned int makeWord_Word(unsigned int w)
{ 
  return w; 
}

unsigned int makeWord_highByte_lowByte(unsigned char h, unsigned char l)
{
  return (h << 8) | l;
}