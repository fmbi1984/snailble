/*
 * ucs.c
 *
 * Created: 24/02/2014 21:53:12
 *  Author: VendWatch
 */
/*
** Ucs.c
** Low-level dex access functions
**
**
*/

/*
**
** UCS First handshake, sent by Master to initiate communications
**
**	unsigned char  u_commid[10];   ASCII 10 digit UCS comm ID.
**	unsigned char  u_opreq;        Operatioin request, "S" to send,
**                                    "R" to receive.
**	unsigned char  u_revlevel[6];   UCS revision level "R01L01"
**	unsigned char  u_dle_etx[2];    First handshake terminator.
**	unsigned char  u_crc[2];        CRC
**
** UCS second handshake, sent by slave in response to first HS
**
**	unsigned char  u_respcode[2];    Response code.
**	unsigned char  u_commid[10];     ASCII 10 digit UCS comm ID.
**	unsigned char  u_revlevel[6];    UCS revision level "R01L01"
**	unsigned char  u_dle_etx[2];     Second handshake terminator.
**	unsigned char  u_crc[2];         CRC
*/
#include "ucs.h"

#include "rgbled.h"
#include "serial.h"
#include "ble_uart.h"

#include "wiring.h"

#define USE_BLE

#if defined USE_BLE
#include "ble.h"
#else
//#error "User is stoopid!"
#endif


#include <stdio.h>
#include <math.h>

char sDexBuffer[DEXLENGTH];

int memory_readBytes(uint32_t address, char *data, int length);
int memory_writeBytes(uint32_t address, char *data, uint32_t length);

int memory_readBytes(uint32_t address, char *data, int length)
{
  memcpy(&data[0], (const char *)&sDexBuffer[address], length);
  return 1;
}

int memory_writeBytes(uint32_t address, char *data, uint32_t length)
{
  memcpy(&sDexBuffer[address], (const char *)data, length);
  return 1;
}


int ft = 0;

//char bitbuffer[PACKETCACHESIZE];

char *RX_STATUS_TEXT[20]= {"Undefined","RX_OK","RX_TIMEOUT","RX_GOTEOT",
                           "RX_GOTENQ","RX_BADCRC","RX_TOOBIG","RX_NOCHARS","RX_NODXS", "RX_NODXE",
                           "DISABLED","RX_HANDSHAKE_FAILED", "RX_SLAVE_RX_FAILED",
                           "RX_MASTER_RX_FAILED","RX_COMM_FAILED", "RX_POWER_FAILED",
                           "RX_POWER_RESTORED","RX_BATTERY_FAILED","RX_VM_IS_MASTER",
                           "RX_GOT_WRONG_ACK"
                          };


char packetcache[PACKETCACHESIZE];
int packetcachelen=0;
int packetcacheidx=0;

int needToSendPassword=ucsTRUE;

char*		gsUcsRevLev= "R01L01";
char*		gsUcsCommId= "1111111111";
char*		gsUcsRespCode = "00";

int	SIZEOFHAND1	=23;

/*
** UCS handshake buffer.
*/

char		gsHandShake1[HANDSHAKEBUFFERSIZE];

char ack0Data[4];
char ack1Data[4];
char*		g_ucsAcks[2]= {ack0Data,ack1Data};

int iAck=C_ACK0;

char UCS_COMMAND_RECEIVE= 'R';
char UCS_COMMAND_SEND= 'S';

int UCS_Initialize()
{
	logg(LOG_INFO |  LOG_DEXLOG,"UCS_Initialize()");
    UCS_InitAcks();
    return GPRS_OK;
}

int UCS_InitAcks()
{
    char *cp;
		logg(LOG_INFO |  LOG_DEXLOG,"UCS_InitAcks()");
    cp = g_ucsAcks[C_ACK0];
    *cp = C_DLE;
    cp[1]='0';
    cp[2]='\0';
    cp = g_ucsAcks[C_ACK1];
    *cp = C_DLE;
    cp[1]='1';
    cp[2]='\0';
    return GPRS_OK;
}

int UCS_SendENQ()
{
		char tbuf[2];
		int retval;
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendENQ()");
    /*
    ** Pull all extra stuff from serial port.
    */
    clearSerialPort(DEX_PORT);
    tbuf[0]=C_ENQ;

	if(ft==1)
	{
		serial_sleep();
		delay(10);
		serial_wakeup();
		delay(10);
		serial_sleep();
		delay(100);
		serial_wakeup();
		delay(100);


	}
	uts(tbuf,1);

	//trace_printf("UCS_SendENQ()\r\n");
	//trace_printf("%u ms\r\n", stopwatch_elapsed());

    iAck=C_ACK0;
    retval=UCS_waitForAck(C_ACK0);
	if(ft==1 && retval ==1)
	{
		ft = 0;
	}

	//trace_printf("UCS_waitForAck(C_ACK0)\r\n");
	//trace_printf("%u ms\r\n", stopwatch_elapsed());

    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendENQ() status = %s",  RX_STATUS_TEXT[retval]);
    return retval;
}

int UCS_WaitForENQ()
{
    char wbuf[2];
    int retval;
    wbuf[0]=C_ENQ;

	//trace_printf("serialSendReceive(NULL, 0,wbuf, 1,1, C_RESPONSE_TIMEOUT) begin\r\n");
	//trace_printf("%u ms\r\n", stopwatch_elapsed());

    retval=serialSendReceive(NULL, 0,wbuf, 1,1, C_RESPONSE_TIMEOUT);

	//trace_printf("serialSendReceive(NULL, 0,wbuf, 1,1, C_RESPONSE_TIMEOUT) end\r\n");
	//trace_printf("%u ms\r\n", stopwatch_elapsed());

    logg(LOG_INFO |  LOG_DEXLOG,"UCS_WaitForENQ() returning %s", RX_STATUS_TEXT[retval]);
    return retval;
}

int UCS_waitForAck(int acknum)		// acknum is 0 or 1
{
    int got_dle=0;
    char thischar;
    int status=GPRS_OK;
    int bDone=ucsFALSE;
    TIMER tTimeout;
    startTimer(&tTimeout,C_RESPONSE_TIMEOUT);
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_waitForAck()");
    while (!bDone)
    {
        if (timerExpired(&tTimeout))
        {
        	bDone=ucsTRUE;
        	status = RX_TIMEOUT;
        }
        else
        {
	        status=serialReadChar(&thischar, C_RESPONSE_TIMEOUT/4);
	        if(status == GPRS_OK)
	        {
	            if(got_dle)
	            {

					switch(thischar)
					{
						case C_WACK:	// wack
							logg(LOG_INFO |  LOG_DEXLOG,"Got W_ACK\r\n");
							msec_delay(50L);		// short pause
							thischar=C_ENQ;
							uts(&thischar,1);
							startTimer(&tTimeout,C_RESPONSE_TIMEOUT);  // restart response timer
							break;
						case '0':
						case '1':
							if((thischar & 1) != acknum)
							{
								status= RX_WRONGACK;		// got wrong ack
							}
							bDone=ucsTRUE;		// either way, satisfied the wait.
							break;
						default:
							logg(LOG_INFO |  LOG_DEXLOG,"Got incorrect character : 0x%2.2x\r\n", 0x0ff & thischar);
							break;
					}
	                got_dle=0;
	            }
	            else
	            {
					switch(thischar)
					{
						case C_DLE:
							logg(LOG_INFO |  LOG_DEXLOG,"Got DLE.");
							got_dle=1;
							break;
						case C_ENQ:
							logg(LOG_INFO |  LOG_DEXLOG,"Got ENQ.");
							status= RX_GOTENQ;
							bDone=ucsTRUE;
							break;
						default:
							logg(LOG_INFO |  LOG_DEXLOG,"Got incorrect character : 0x%2.2x\r\n", 0x0ff & thischar);
						break;
					}
	            }
	        }
	      }
    }
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_waitForAck() Returning %s",RX_STATUS_TEXT[status]);
    return status;
}

/*
** The packet is pretty much the same for the different handshakes, with the
** exception of the Command byte.  It is not included for Response Packets, and
** set to 'S' when we will send DEX, set to 'R' when we will receive dex.
** Also, Response packets have a response code in the first 2 bytes, normally '00'.
*/

int UCS_InitHandShakes( char *respcode,char bSend )
{
    int	nCrc = 0;
    int len;
    int respcodelen=0;
    char wbuf[2];
    wbuf[0]=bSend;
    wbuf[1]=0;
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_InitHandShakes() bSend=%s",wbuf);
    memset(gsHandShake1,0,HANDSHAKEBUFFERSIZE);
    if(respcode)
    {
	    respcodelen=strlen(respcode);
	  }
    gsHandShake1[0] = C_DLE;
    gsHandShake1[1] = C_SOH;
    gsHandShake1[2] = 0;
    if(respcodelen)
    {
        strcat(gsHandShake1,respcode);
    }
    strcat(gsHandShake1,gsUcsCommId);
    if(!respcodelen)		// if no response code, must be a Master packet
    {
		len=strlen(gsHandShake1);
        gsHandShake1[len++]=bSend;
		gsHandShake1[len] = 0;
    }
    strcat(gsHandShake1,gsUcsRevLev);
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_InitHandShakes()  Handshake Packet = %s", gsHandShake1);
    len=strlen(gsHandShake1);
    gsHandShake1[len++] = C_ETX;
    gsHandShake1[len++] = C_ETX;
// Determine the crc for the packet and ETX
    nCrc = calc_crc((unsigned char *)&gsHandShake1[2],len-3);  // include the ETX, but not SOH or DLEs.
    gsHandShake1[len-2] = C_DLE;

    // Set the crc within the packet
    gsHandShake1[len++] = (nCrc & 0xFF);
    gsHandShake1[len++] = ((nCrc >> 8) & 0xFF);

    SIZEOFHAND1=len;
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_InitHandShakes()  SIZEOFHAND1 = %d", SIZEOFHAND1);
    return len;
}

int UCS_ReadDexData(
		char* sTempBuffer,int nTempBufSize, int* pnDexLength)
{
    int	status = GPRS_OK;
    int	bDone = ucsFALSE;
    uint32_t	nLength = 0;
    int	nBytesRead;
    int 	retrycount=4;
    int	iAck = C_ACK1;
		int k;

    //logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexData()  Size: %d",nDexLength);

    while (bDone == ucsFALSE)
    {
        status=rxpacket(sTempBuffer,nTempBufSize,C_RESPONSE_TIMEOUT,
                        C_RX_TIMEOUT,&nBytesRead);
        if(status == RX_OK)   // got a normal packet
        {
            uts(g_ucsAcks[iAck], 2);
            iAck=(1-iAck);
            k=nLength;
            nLength+=nBytesRead;
/*
** Updated the count to keep track of the ucsTRUE size of the dex, but
** Only copy into the buffer if the total received length < max size.
*/
            //if(nLength < DEXLENGTH)
            //{
                //memcpy(&sDexBuffer[k], (const char *)sTempBuffer, nBytesRead);

				memory_writeBytes(k, sTempBuffer, nBytesRead);

			//}
            logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexData: Got Packet, Length=%d, send ack#%d",nBytesRead, iAck);
            retrycount=4;
        }
        else if(status == RX_BADCRC)   // got a bad CRC
        {
            uts(g_ucsAcks[1-iAck], 2);	// send last ack
            logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexData: Bad CRC");
            retrycount=4;
        }
        else if(status == RX_GOTEOT)  // we are done!
        {
            logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexData: Got EOT");
            status=GPRS_OK;
            bDone=ucsTRUE;
            break;
        }
/*
** If the other side explicitly sends an ENQ we send the previous ACK,
** Or, if we get nothing, we assume we missed an ENQ or the other side
** is somehow waiting for something from us.
** Either way, send the last ACK to kickstart the transfer.
*/
        else if((status == RX_GOTENQ) || (status == RX_TIMEOUT))
        {
            packetcacheidx=0;
            packetcachelen=0;
            clearSerialPort(DEX_PORT);	// get any old data off the queue
            if(retrycount>0)
            {
                logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexData: Got ENQ Re-Sending ACK #%d",iAck);
                uts(g_ucsAcks[1-iAck], 2);
                retrycount--;
            }
            else
            {
                logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexData: Got ENQ, aborting");
                nLength=0;
                status=RX_TIMEOUT;
                bDone=ucsTRUE;
                break;
            }
        }
        else
        {
            logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexData: Got unknown result : %d", status);
            nLength=0;
            bDone=ucsTRUE;
            break;
        }
    }

    logg(LOG_INFO |  LOG_DEXLOG,"End:  UCS_ReadDexData  Bytes: %d",nLength);
    //if(nLength > DEXLENGTH)
    //{
    //    status=RX_TOOBIG;
    //    logg(LOG_INFO |  LOG_DEXLOG," UCS_ReadDexData  Dex Too Big!");
    //}
	*pnDexLength=nLength;
    return status;
}

int FindBytesInBlock(char* pData, int iMaxBlock)
{
	char*				pStart;
	char*				pCrLf;
	char*				pLastCrLf = NULL;
	int					iCnt = iMaxBlock;

	pStart = pData;

	do {
		pCrLf = strpbrk(pStart, "\r\n");
		if (pCrLf) {
			while ((*pCrLf == '\r') || (*pCrLf == '\n')) {
				++pCrLf;
			}
			iCnt = pCrLf - pData;
			if (iCnt >= iMaxBlock) {
				if (pLastCrLf) {
					iCnt = pLastCrLf - pData;
				}
				else {
					iCnt = iMaxBlock;
				}
				break;
			}
			pLastCrLf = pCrLf;
			pStart = pCrLf;
		}
	} while (pCrLf);

	return iCnt;
}
/*
** Not compressing anything right now, so we can use the bitbuffer
** in the compress library for temp storage
*/
#if 1	// Changed by Dave Patton on 6/16/15
const char csDexStart[] = "DXS*1111111111*VA*V1/1*1**\r\nST*001*0001\r\n";
const char csDexEnd[] = "\r\nSE*50*0001\r\nDXE*1*1\r\n";
#define C_SENDING_START	(0)
#define C_SENDING_DATA	(1)
#define C_SENDING_END	(2)
int UCS_SendDexPacket(char* sDex)
{
	int status = GPRS_OK;
	unsigned short nCrc	= 0;
	int iStartLen = strlen(csDexStart);
	int iDexLen = strlen(sDex);
	int iEndLen = strlen(csDexEnd);
	int iTotalLen = iStartLen + iDexLen + iEndLen;
	char wbuf[2];
	char crcbuf[2];
	char startblock = 1;
	char cBlock = C_SENDING_START;
	int iBlockBytesSent = 0;
	int iFrameBytesSent = 0;
	int iAckToGet = C_ACK1;
	char* pDataToSend = (char*)csDexStart;
	int ii;
	int iMaxBytesPerBlock = C_UCS_DL_MTU;

	logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendDexPacket: Sending Dex Packet");

	status = UCS_SendENQ();
	if (status != GPRS_OK) {
		return status;
	}

	if (iTotalLen > C_UCS_DL_MTU) {
		iMaxBytesPerBlock = FindBytesInBlock(sDex, (C_UCS_DL_MTU - iStartLen));
		iMaxBytesPerBlock += iStartLen;
	}

	for (ii=0; ii<iTotalLen; ++ii) {
		if (startblock) {
			wbuf[0] = C_DLE;
			wbuf[1] = C_STX;
			uts(wbuf, 2);
			startblock = 0;

			if (cBlock == C_SENDING_DATA) {
				iMaxBytesPerBlock = FindBytesInBlock(pDataToSend, (C_UCS_DL_MTU - iEndLen));
				iMaxBytesPerBlock += iEndLen;
			} else if (cBlock != C_SENDING_START) {
				iMaxBytesPerBlock = C_UCS_DL_MTU;
			}
		}

		// Send the byte
		uts(pDataToSend, 1);
		++iBlockBytesSent;

		// Add the byte to the CRC
		nCrc = calc_crc_with_feed((unsigned char *)pDataToSend, 1, nCrc);

		// Point to the next byte to send..
		++pDataToSend;

		switch (cBlock) {
			case C_SENDING_START:
			if (iBlockBytesSent >= iStartLen) {
				pDataToSend = (char*)sDex;
				iBlockBytesSent = 0;
				cBlock = C_SENDING_DATA;
			}
			break;
			case C_SENDING_DATA:
			if (iBlockBytesSent >= iDexLen) {
				pDataToSend = (char*)csDexEnd;
				iBlockBytesSent = 0;
				cBlock = C_SENDING_END;
			}
			break;
			case C_SENDING_END:
			break;
		}

		++iFrameBytesSent;
		if ((iFrameBytesSent >= iMaxBytesPerBlock) || (ii == (iTotalLen-1))) {
			// We need to end with an ETB or an ETX.  If this is the very last block
			// it should be an ETX; otherwise, it should be an ETB.
			if (ii == (iTotalLen-1)) {
				wbuf[1] = C_ETX;
			} else {
				wbuf[1] = C_ETB;
			}
			wbuf[0] = C_DLE;
			uts(wbuf, 2);

			// Add the last byte to the CRC but don't add the DLE
			nCrc = calc_crc_with_feed((unsigned char *)&wbuf[1], 1, nCrc);
			// Send the CRC..
			crcbuf[0] = (nCrc & 0xFF);
			crcbuf[1] = ((nCrc >> 8) & 0xFF);
			uts(crcbuf, 2);

			// Reset the Block CRC so it gets calculated correctly for the next block..
			nCrc = 0;

			// This is the end of this frame so reset the frame bytes sent..
			iFrameBytesSent = 0;

			startblock = 1;

			status = UCS_waitForAck(iAckToGet);
			if (status != GPRS_OK) {
				return status;
			}
			if (iAckToGet == C_ACK1) {
				iAckToGet = C_ACK0;
			} else {
				iAckToGet = C_ACK1;
			}
		}
	}

	// Send the EOT..
	if (status == GPRS_OK) {
		msec_delay(40L);	// wait 40 msecs to make sure the other side is ready to recv
		wbuf[0] = C_EOT;
		uts(wbuf, 1);
	}

	return status;

	/*
	nCrc = calc_crc_with_feed((unsigned char *)csDexStart, iStartLen, nCrc);
	nCrc = calc_crc_with_feed((unsigned char *)sDex, iDexLen, nCrc);
	nCrc = calc_crc_with_feed((unsigned char *)csDexEnd, iEndLen, nCrc);
	wbuf[0] = C_ETX;
	nCrc = calc_crc_with_feed((unsigned char *)wbuf, 1, nCrc);	// Add the ETX to the CRC, but not the DLE
	crcbuf[0] = (nCrc & 0xFF);
	crcbuf[1] = ((nCrc >> 8) & 0xFF);

	// Send the ENQ and wait for DLE0
	logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendDexPacket:  Sending ENQ waiting for DLE 0");
	status=UCS_SendENQ();

	if (status == GPRS_OK)
	{
		logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendDexPacket: Sending Packet");
		//uts(gsPacket, nLength);
		wbuf[0] = C_DLE;
		wbuf[1] = C_STX;
		uts(wbuf, 2);
		uts((char*)csDexStart, iStartLen);
		uts((char*)sDex, iDexLen);
		uts((char*)csDexEnd, iEndLen);
		wbuf[0] = C_DLE;
		wbuf[1] = C_ETX;
		uts(wbuf, 2);
		uts(crcbuf, 2);
		status = UCS_waitForAck(C_ACK1);
		logg(LOG_INFO |  LOG_DEXLOG,"Return = %s",RX_STATUS_TEXT[status]);
	}

	if (status == GPRS_OK)
	{
		logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendDexPacket: Got ACK, Sending EOT");
		msec_delay(40L);	// wait 40 msecs to make sure the other side is ready to recv
		wbuf[0] = C_EOT;
		uts(wbuf, 1);
	}
	return status;
	*/
}
#else
int UCS_SendDexPacket(char* sDex)
{
    int status = GPRS_OK;
    char *gsPacket;
    int nCrc	= 0;
    int nLength	= 0;
    char wbuf[10];
    gsPacket=bitbuffer;
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendDexPacket: Sending Dex Packet");
    gsPacket[0] = C_DLE;
    gsPacket[1] = C_STX;
    strcpy( gsPacket+2, "DXS*1111111111*VA*V1/1*1**\r\nST*001*0001\r\n" );
    strcat( gsPacket, sDex );
    strcat( gsPacket, "\r\nSE*50*0001\r\nDXE*1*1\r\n" );

    nLength = strlen(gsPacket);

// 02DXS, nLength = 5
    gsPacket[nLength++] = C_ETX;
    gsPacket[nLength++] = C_ETX;
// 02DXSEE, nLength = 7
    nCrc = calc_crc((unsigned char *)&gsPacket[2],nLength-3);
    gsPacket[nLength-2] = C_DLE;
// 02DXSdE, nLength = 7

    gsPacket[nLength++] = (nCrc & 0xFF);
    gsPacket[nLength++] = ((nCrc >> 8) & 0xFF);

    // Send the ENQ and wait for DLE0
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendDexPacket:  Sending ENQ waiting for DLE 0");
    status=UCS_SendENQ();

	if (status == GPRS_OK)
    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendDexPacket: Sending Packet");
        uts(gsPacket, nLength);
        status = UCS_waitForAck(C_ACK1);
        logg(LOG_INFO |  LOG_DEXLOG,"Return = %s",RX_STATUS_TEXT[status]);
    }

    if (status == GPRS_OK)
    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendDexPacket: Got ACK, Sending EOT");
        msec_delay(40L);	// wait 40 msecs to make sure the other side is ready to recv
        wbuf[0] = C_EOT;
        uts(wbuf, 1);
    }
    return status;
}
#endif

/*
** Password must be less than 13 chars
*/
int UCS_SendPassword(char* 	sDexPassword)
{
    char gsPasswdPacket[20];
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendPassword() : Sending Dex Passwd '%s'",sDexPassword);
    strcpy( gsPasswdPacket, "SD1*");
    if(strlen(sDexPassword) > 12)
    {
    	sDexPassword[12]='\0';
    }
    strcat( gsPasswdPacket, sDexPassword );
    return UCS_SendDexPacket(gsPasswdPacket);
}

/*
** Send ENQ and Master handshake, either to send a command or
** to instruct the VM to send Dex.
*/

int UCS_SendMasterHandshake(char bSend, int allowRemoteMaster)
{
		char dexworkbuf[HANDSHAKEBUFFERSIZE];
    int status = GPRS_OK;
    int bDone	= ucsFALSE;
    int loop	= 0;
    int rxcount=0;
    int max_loop_retries;
    char wbuf[2];
		wbuf[0]=bSend;
		wbuf[1]='\0';
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake()  bSend=%s, allowMaster=%d",wbuf,allowRemoteMaster);
    UCS_InitHandShakes(NULL,bSend);			// No response code, Command in bSend

    // Send the ENQ and wait for ACK

    // Loop 5 times
    max_loop_retries=MAX_ENQ_TRIES;
   	if(allowRemoteMaster==ucsFALSE)
		{
			max_loop_retries=MAX_ENQ_TRIES_EXTENDED;
		}
    while ( (bDone == ucsFALSE) && ( loop < max_loop_retries )  )
    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake:  Sending ENQ and waiting for ACK");

		//stopwatch_begin();
		//stopwatch_start();
		//trace_printf("UCS_SendENQ() begin %u ms\r\n", stopwatch_elapsed());
	    status = UCS_SendENQ();
	    //trace_printf("UCS_SendENQ() end %u ms\r\n", stopwatch_elapsed());

        if (status == GPRS_OK)
        {
            bDone=ucsTRUE;
        }
        else if(status == RX_GOTENQ)
        {
        	if(allowRemoteMaster)
        	{
        		bDone=ucsTRUE;
        	}
        	else
        	{
		        logg(LOG_INFO |  LOG_DEXLOG,"Got ENQ instead of ACK, but not allowing SLAVE MODE.  Retry the ENQ.");
        	}
        }
        loop++;
    }

/*
** If we get an ENQ in response to our ENQ, the VM wants to be in Master mode.
** We should respond with an ACK, and then get a version packet that will probably
** have an 'S' command in it, specifying the VM wants to send us a packet.
*/

    if(status == RX_GOTENQ)
    {
        logg(LOG_INFO |  LOG_DEXLOG,"Got ENQ instead of ACK - we are assuming SLAVE MODE.");
        return RX_REMOTE_IS_MASTER;
    }

// We got the ACK for the ENQ. Send the Handshake packet
// and wait for ACK

    else if (status == GPRS_OK)
    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake:  Sending Initial Handshake packet and waiting for ACK");

		//trace_printf("uts(gsHandShake1, SIZEOFHAND1) begin %u ms\r\n", stopwatch_elapsed());
        uts(gsHandShake1, SIZEOFHAND1);
		//trace_printf("uts(gsHandShake1, SIZEOFHAND1) end %u ms\r\n", stopwatch_elapsed());
		//trace_printf("UCS_waitForAck(C_ACK1) begin %u ms\r\n", stopwatch_elapsed());
        status = UCS_waitForAck(C_ACK1);
		//trace_printf("UCS_waitForAck(C_ACK1) end %u ms\r\n", stopwatch_elapsed());
        logg(LOG_INFO |  LOG_DEXLOG,"Status=%s",RX_STATUS_TEXT[status]);
    }

    /*
    ** An error up to this point is either a timeout, or an internal error of some kind.
    ** If we sent the handshake, but something else fails down the road, we will report
    ** it as just a handshake error.
    ** So, any error we have until this point, go ahead and return the real error.
    */
    if(status != GPRS_OK)
    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake:  Comm error.  Returning Status=%s",
                   RX_STATUS_TEXT[status]);
        return status;
    }

    // Send the EOT and wait for ENQ
    if (status == GPRS_OK)
    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake:  Sending EOT and waiting for ENQ");
        msec_delay(40L);	// wait 40 msecs to make sure the other side is ready to recv
        wbuf[0]=C_EOT;
        uts(wbuf,1);
        status=UCS_WaitForENQ();
        logg(LOG_INFO |  LOG_DEXLOG,"Status=%s",RX_STATUS_TEXT[status]);
    }

// GPRS_OK from UCS_WaitForENQ() means we got the ENQ.
// Remote wants to send us a Response packet.
// Send the ACK and wait for the Response Packet

    if (status == GPRS_OK)
    {
        status = RX_BADCRC;
        loop=0;
        while ( (status == RX_BADCRC) && ( loop < MAX_ENQ_TRIES )  )
        {
            logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake:  Sending ACK and waiting for second handshake packet");
            uts(g_ucsAcks[C_ACK0], 2);		// ACK0 for the ENQ
            logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake:  Recv packet");
            status=rxpacket(dexworkbuf, HANDSHAKEBUFFERSIZE-1,
            			C_RESPONSE_TIMEOUT, C_RX_TIMEOUT, &rxcount);
            logg(LOG_INFO |  LOG_DEXLOG,"Status=%s",RX_STATUS_TEXT[status]);
            loop++;
        }
    }

    // Send the ACK and wait for EOT, ENQ
    if (status == GPRS_OK)
    {
        wbuf[0] = C_EOT;

        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake:  Sending ACK1 and waiting for EOT");

		//trace_printf("serialSendReceive(g_ucsAcks[C_ACK1], 2,wbuf, 1,1, C_RESPONSE_TIMEOUT) begin\r\n");
		//trace_printf("%u ms\r\n", stopwatch_elapsed());
        status = serialSendReceive(g_ucsAcks[C_ACK1], 2,wbuf, 1,1, C_RESPONSE_TIMEOUT);
        //trace_printf("serialSendReceive(g_ucsAcks[C_ACK1], 2,wbuf, 1,1, C_RESPONSE_TIMEOUT) end\r\n");
        //trace_printf("%u ms\r\n", stopwatch_elapsed());

		logg(LOG_INFO |  LOG_DEXLOG,"Status=%s",RX_STATUS_TEXT[status]);
        if(status == GPRS_OK)
        {
            if(bSend == UCS_COMMAND_RECEIVE)	// are we expecting to receive?
            {
                logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake:  Waiting for ENQ");
                status = UCS_WaitForENQ();
                logg(LOG_INFO |  LOG_DEXLOG,"Status=%s",RX_STATUS_TEXT[status]);
                if(status == GPRS_OK)
                {
                    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake: sending ACK0 for ENQ.");
                    uts(g_ucsAcks[C_ACK0], 2);
                }
            }
        }
    }

    if(status != GPRS_OK)	// if we fail here, it means we need to send password, probably
    {
        status = RX_HANDSHAKE_FAILED;
    }
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendMasterHandshake:  Returning Status=%s",RX_STATUS_TEXT[status]);
    return status;
}

int UCS_SendSlaveHandshake( )
{
    int status = GPRS_OK;
    int bDone	= ucsFALSE;
    int loop	= 0;
    char wbuf[2];

    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendSlaveHandshake() start");

    UCS_InitHandShakes(gsUcsRespCode,0);		// response code 00 is Ack

    // Send the ENQ and wait for ACK

    // Loop 5 times
    while ( (bDone == ucsFALSE) && ( loop < MAX_ENQ_TRIES )  )
    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendSlaveHandshake:  Sending ENQ and waiting for ACK");
        status = UCS_SendENQ();
        if ((status == GPRS_OK) || (status == RX_GOTENQ))
        {
            bDone=ucsTRUE;
        }
        loop++;
    }

    // Send the Receive Handshake packet
    // and wait for ACK
    if (status == GPRS_OK)
    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendSlaveHandshake:  Sending Initial Handshake packet and waiting for ACK");
        uts(gsHandShake1, SIZEOFHAND1);
        status = UCS_waitForAck(C_ACK1);
        logg(LOG_INFO |  LOG_DEXLOG,"Status=%s",RX_STATUS_TEXT[status]);
    }

    // Send the EOT
    if (status == GPRS_OK)
    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendSlaveHandshake:  Sending EOT");
        msec_delay(40L);	// wait 40 msecs to make sure the other side is ready to recv
        wbuf[0]=C_EOT;
        uts(wbuf, 1);
    }
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_SendSlaveHandshake:  Returning Status=%s",RX_STATUS_TEXT[status]);
    return status;
}

/*
** Common code for the slave mode handshake.
*/

/*
** We execute this function when the remote assumes Master status.
** We have received an ENQ in response to our ENQ.
** The first thing we will do is send the ACK for the ENQ,
** then receive the handshake packet, then send our own response
** packet.
*/

int UCS_ReadDexSlave(
    int*		pnDexLength,
    char*		sTempBuffer,
    int		nTempBufSize,
    char*   password)
{
    int	status = GPRS_OK;
    int rxcount=0;
    int loop	= 0;
    int k;

    logg(LOG_INFO |  LOG_DEXLOG,"Enter UCS_ReadDexSlave()");
    status=RX_TIMEOUT;
/*
** Loop for 3 tries, or until we get the handshake packet.
*/
    while((loop < 3) && (status != GPRS_OK))
    {
        loop++;
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexSlave() : Sending ACK for the ENQ");
        uts(g_ucsAcks[C_ACK0], 2);
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexSlave:  Recv packet");
        status=rxpacket(sTempBuffer, HANDSHAKEBUFFERSIZE-1, C_RESPONSE_TIMEOUT,
                        C_RX_TIMEOUT, &rxcount);
        logg(LOG_INFO |  LOG_DEXLOG,"rxpacket Status=%s",RX_STATUS_TEXT[status]);
    }
    /*
    ** When we get the good packet, look for 'SR01L01' or 'RR01L01'
    ** to tell us if we are going to receive ('s') or send('r') a dex.
    ** The command is from the Master's perspective.
    ** If we get an 'R', we must send the password.
    ** If we get an 'S', the other side will send us a dex.
    */
    if(status == RX_OK)		// did we get a good packet?
    {
        loop=0;
        while((status != RX_GOTEOT) && (loop<3))
        {
            loop++;
            uts(g_ucsAcks[C_ACK1], 2);
/*
** The next call should result in us receiving an EOT without a packet.
*/
            status=rxpacket(sTempBuffer, 270, C_RESPONSE_TIMEOUT,
                            C_RX_TIMEOUT, &k);
        }
        if(status != RX_GOTEOT)
        {
            return RX_TIMEOUT;
        }
/*
** This should be a Master handshake, consisting of a 10-character ID
** Followed by a single command 'S' or 'R',
** Followed by the Revision level 'R01L01'.
*/
				status = RX_HANDSHAKE_FAILED;		// assume handshake format error.
        if(rxcount >= 17)
        {
            if(sTempBuffer[10]=='S')
            {
            		logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexSlave : 'S' command found - we will receive dex.");
                status=UCS_SendSlaveHandshake();
                if(status==GPRS_OK)
                {
                    status=UCS_WaitForENQ();
                }
                if(status==GPRS_OK)
                {
                    uts(g_ucsAcks[C_ACK0], 2);		// ack the ENQ
                    iAck=C_ACK1;
                    status = UCS_ReadDexData(
                        sTempBuffer, nTempBufSize, pnDexLength);
                }
            }
            else if(sTempBuffer[10]=='R')
            {
            		logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDexSlave : 'R' command found - we will Send password.");
                status=UCS_SendSlaveHandshake();
                if(status==GPRS_OK)
                {
                    UCS_SendPassword(password);
                }
                if(status==GPRS_OK)
                {
                    status=UCS_WaitForENQ();
                }
                if(status==GPRS_OK)		// yes, it is a recursive call.  should be fine.
                {
                    status=UCS_ReadDexSlave(pnDexLength,sTempBuffer,
                                            nTempBufSize,password);
                }
            }
        }
    }
    logg(LOG_INFO |  LOG_DEXLOG,"End:UCS_ReadDexSlave  Result: %s", RX_STATUS_TEXT[status]);
    return status;
}

/*
** for efficiency, we will try to read the dex with password first, unless
** we have a pushbutton event.
*/

int UCS_ReadDex(
    int*		pnDexLength,
    char*		sTempBuffer,
    int		nTempBufSize,
    char*   password)
{
    int	status = RX_HANDSHAKE_FAILED;
    //logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDex() Size: %d",nDexBufSize);
    UCS_Initialize();
	ft = 1;
    // Initialize the dex length
    *pnDexLength = 0;

// the door is open so try without password.

		logg(LOG_INFO |  LOG_DEXLOG,"Try to read dex without password.");
	    status = UCS_SendMasterHandshake(UCS_COMMAND_RECEIVE, ucsTRUE);  // will allow remote to be master
	    if(status == RX_REMOTE_IS_MASTER)		// we got an ENQ in response to our ENQ
	    {
        return UCS_ReadDexSlave(pnDexLength,sTempBuffer,
                                nTempBufSize,password);
	    }
	    if (status == GPRS_OK)
	    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDex:  Reading Dex Data");
        status = UCS_ReadDexData(
                                 sTempBuffer, nTempBufSize,pnDexLength);
	    }

    if(status != GPRS_OK)		// button not pushed or didn't get dex, use password.
    {
        if(password == 0)		// no password?  use default
        {
	        logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDex:  Using defaul Password : 000000");
        	password="000000";
        }
        if(strlen(password) > 12)		// make sure password is not too big
        {
        	password[12]='\0';
        }
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDex:  Sending Password : %s",password);
        status = UCS_SendMasterHandshake(UCS_COMMAND_SEND, ucsFALSE);  // not allowing remote to be master
		    if(status == RX_REMOTE_IS_MASTER)		// we got an ENQ in response to our ENQ
				{
					return UCS_ReadDexSlave(pnDexLength,sTempBuffer,
                        nTempBufSize,password);
				}
        if(status == GPRS_OK)
        {
            status = UCS_SendPassword( password);
        }
        if(status == GPRS_OK)
        {
            status = UCS_SendMasterHandshake(UCS_COMMAND_RECEIVE, ucsTRUE);  // allow remote to be master
				    if(status == RX_REMOTE_IS_MASTER)		// we got an ENQ in response to our ENQ
	    			{
        			return UCS_ReadDexSlave(pnDexLength,sTempBuffer,
                                nTempBufSize,password);
	    			}
        }
        if (status == GPRS_OK)
        {
            logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDex:  Reading Dex Data");
            status = UCS_ReadDexData(
                                     sTempBuffer, nTempBufSize,pnDexLength);
            if(status == GPRS_OK)		// we got dex after sending password.
            {
            	needToSendPassword=ucsTRUE;
            }
        }
    }
/*
** If we failed to get dex, but the button was not pressed, and the 'need password'
** flag is set, then we have only tried once with a password.
** Try one more time without the password (the door may be open, no button press).
*/
    if(status != GPRS_OK)
    {
			logg(LOG_INFO |  LOG_DEXLOG,"salesman_visit is ucsFALSE, failed with password, try without password.");
	    status = UCS_SendMasterHandshake(UCS_COMMAND_RECEIVE, ucsTRUE);  // allow remote to be master
	    if(status == RX_REMOTE_IS_MASTER)		// we got an ENQ in response to our ENQ
 			{
     			return UCS_ReadDexSlave(pnDexLength,sTempBuffer,
                             nTempBufSize,password);
 			}
	    if (status == GPRS_OK)
	    {
        logg(LOG_INFO |  LOG_DEXLOG,"UCS_ReadDex:  Reading Dex Data");
        status = UCS_ReadDexData(
                                 sTempBuffer, nTempBufSize,pnDexLength);
	    }
    }
    logg(LOG_INFO |  LOG_DEXLOG,"End: UCS_ReadDex  Result: %s", RX_STATUS_TEXT[status]);
    return status;
}

int UCS_send_new_password(char *pwd, char *newpwd)
{
	int status;
	char tbuf[40];
  logg(LOG_INFO |  LOG_DEXLOG,"enter UCS_send_new_password(%s,%s)", pwd,newpwd);
	strcpy(tbuf,"SD1*");
	strcat(tbuf,pwd);
	strcat(tbuf,"*");
	strcat(tbuf,newpwd);
	strcat(tbuf,"\r\n");
	status = UCS_send_data(pwd, tbuf);
/*
	status = UCS_SendMasterHandshake(UCS_COMMAND_SEND);
	if(status == GPRS_OK)
	{
		status= UCS_SendDexPacket(tbuf);
	}
*/
	return status;
}

int UCS_send_data(char *pwd, char *data)
{
    int status;
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_send_data() : %s", data);
    status = UCS_SendMasterHandshake(UCS_COMMAND_SEND, ucsFALSE);  // we must be the master
    if(status == RX_REMOTE_IS_MASTER)
    {
        return status;	// need to add in code to reset as slave.
    }
    else if(status == GPRS_OK)
    {
        status = UCS_SendPassword(pwd);
        if(status == GPRS_OK)
        {
            status = UCS_SendMasterHandshake(UCS_COMMAND_SEND, ucsFALSE);
        }
        if(status == GPRS_OK)
        {
            status= UCS_SendDexPacket(data );
        }
    }
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_send_data() returning %s",  RX_STATUS_TEXT[status]);
    return status;
}

/*
** Try to clear errors and resetable fields
*/

int UCS_clear_resetable_fields(char *pwd)
{
    int status;
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_clear_resetable_fields()");
    status=UCS_send_data(pwd, "MC5*CF");
    return status;
}

int UCS_clear_errors(char *pwd)
{
    int status;
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_clear_errors()");
    status=UCS_send_data(pwd, "MC5*RESET********");
    return status;
}

int UCS_updateVMserials(char *pwd, char *vtpasswd,char *dexdata)
{
    int status=RX_OK;
    char *cp;
    char *cp2;
    char savechar;
    logg(LOG_INFO |  LOG_DEXLOG,"UCS_updateVMserials()");
    cp=dexdata;
    while(*cp)
    {
    		cp2=cp;
    		while(*cp2 && (*cp2 != '\r') && (*cp2 != '\n'))
    			cp2++;
    		savechar=*cp2;
    		*cp2='\0';
		    status=UCS_send_data(pwd, cp);
		    if(status == RX_REMOTE_IS_MASTER)
		    {
		    	return status;
		    }
		    *cp2=savechar;
    		while((*cp2 == '\r') || (*cp2 == '\n'))
    			cp2++;
		    cp=cp2;
		}
		UCS_send_new_password(pwd, vtpasswd);
    return status;
}

/*
**
** Packet related functions
** Requires:
**
*/

/*
** Receive a properly formatted packet :
** DLE SOH ... data ... DLE ETX CRC1 CRC2
** starttimer - how many ms to wait for first char
** chartimer - how many ms to wait for each subsequent char
*/

enum {STATE_WAIT_DLE,
      STATE_WAIT_SOH,
      STATE_RX_CHARS,
      STATE_GOT_DLE,
      STATE_WAIT_CRC1,
      STATE_WAIT_CRC2
     };

int rxpacket(char *buf, int buflen, int starttimeout,
               int chartimeout, int *recvcount)
{
	//stopwatch_begin();
	//stopwatch_stop();
	//stopwatch_reset();
	//stopwatch_start();
	//trace_printf("int rxpacket(...) begin");
	//trace_printf("%u ms\r\n", stopwatch_elapsed());

	int	status = RX_OK;
    int	bDone = ucsFALSE;
    int	nLength = 0;
    TIMER ctimer;
    int mycrc=0;
    int rxcrc=0;
    int rxstate=STATE_WAIT_DLE;
    int j;
    char thischar;
    logg(LOG_INFO |  LOG_DEXLOG,"enter rxpacket  Size: %d",buflen);

    startTimer(&ctimer,starttimeout);
    while (bDone == ucsFALSE)
    {
        while(packetcachelen==packetcacheidx)
        {
            packetcacheidx=0;
            packetcachelen=0;
            if (utrl())
            {
                packetcachelen = utr(packetcache,PACKETCACHESIZE);
            }
            else if(timerExpired(&ctimer))
            {
                status=RX_TIMEOUT;
                bDone=ucsTRUE;
                break;
            }
        }

		//while(packetcacheidx<packetcachelen)
        while((packetcacheidx<packetcachelen) && (bDone == ucsFALSE))
		{
            thischar=packetcache[packetcacheidx++];

			switch(rxstate)
			{
				case STATE_RX_CHARS: //got DLE SOH ? check first for speed
					if(thischar==C_DLE)
					{
						rxstate=STATE_GOT_DLE;
					}
					else
					{
						buf[nLength++]=thischar;    // add to buffer
						if(nLength >= buflen)
						{
							status = RX_TOOBIG;
							bDone=ucsTRUE;
							break;
						}
					}
					break;
				case STATE_GOT_DLE:	//looking for end of packet
					if((thischar==C_ETB) || (thischar==C_ETX))
					{
						buf[nLength]=thischar;    // add to buffer
						//trace_printf("CRC begin. ");
						//trace_printf("%u ms\r\n", stopwatch_elapsed());
						mycrc = calc_crc((unsigned char *)buf,nLength+1);
						//trace_printf("CRC end. ");
						//trace_printf("%u ms\r\n", stopwatch_elapsed());
						rxstate=STATE_WAIT_CRC1;
					}
					else if(thischar==C_SYN)	// filler character
					{
						rxstate=STATE_RX_CHARS;
					}
					else
					{
						rxstate=STATE_RX_CHARS;
						buf[nLength++]=thischar;    // add to buffer
						if(nLength >= buflen)
						{
							status = RX_TOOBIG;
							bDone=ucsTRUE;
							break;
						}
					}
					break;
				case STATE_WAIT_DLE: //waiting for DLE SOH
					if(thischar==C_DLE)
					{
						rxstate=STATE_WAIT_SOH;
					}
					else if(thischar==C_ENQ)
					{
						status = RX_GOTENQ;
						bDone=ucsTRUE;
						break;
					}
					else if(thischar==C_EOT)
					{
						status = RX_GOTEOT;
						bDone=ucsTRUE;
						break;
					}
					break;
				case STATE_WAIT_SOH: //waiting for DLE SOH
					if((thischar==C_SOH) || (thischar==C_STX))
					{
						rxstate=STATE_RX_CHARS;
					}
					else
					{
						rxcrc=0;
						rxstate=STATE_WAIT_DLE;    // start over
					}
					break;
				case STATE_WAIT_CRC1: //got first crc byte
					//trace_printf("STATE_WAIT_CRC1. ");
					//trace_printf("%u ms\r\n", stopwatch_elapsed());
					rxcrc=thischar & 0x0ff;
					rxstate=STATE_WAIT_CRC2;
					logg(LOG_INFO |  LOG_DEXLOG,"Got first CRC character.");

					break;
				case STATE_WAIT_CRC2: //got second crc byte
					//trace_printf("STATE_WAIT_CRC2. ");
					//trace_printf("%u ms\r\n", stopwatch_elapsed());
					rxstate=STATE_WAIT_DLE;
					j=thischar;
					j = (j << 8) & 0xff00;
					rxcrc+=j;
					//trace_printf("rxcrc+=j. ");
					//trace_printf("%u ms\r\n", stopwatch_elapsed());
					logg(LOG_INFO |  LOG_DEXLOG,"Calculated crc = %04x",mycrc);
					logg(LOG_INFO |  LOG_DEXLOG,"Received crc = %04x",rxcrc);
					//mycrc = rxcrc;
					if(mycrc==rxcrc)
					{
						logg(LOG_INFO |  LOG_DEXLOG,"CRC is good.");
						//trace_printf("CRC is good. ");
						//trace_printf("%u ms\r\n", stopwatch_elapsed());
						status=RX_OK;
					}
					else
					{
						logg(LOG_INFO |  LOG_DEXLOG,"CRC values are not equal");
						//trace_printf("CRC values are not equal. ");
						//trace_printf("%u ms\r\n", stopwatch_elapsed());
						status = RX_BADCRC;
					}
					bDone=ucsTRUE;
					break;
			}
        }
	    startTimer(&ctimer,starttimeout);
		//trace_printf("startTimer(&ctimer,starttimeout)\r\n");
		//trace_printf("%u ms\r\n", stopwatch_elapsed());
     }

    if(recvcount != 0)
    {
        *recvcount=nLength;
		//trace_printf("*recvcount=nLength. ");
		//trace_printf("%u ms\r\n", stopwatch_elapsed());
    }

    logg(LOG_INFO |  LOG_DEXLOG,"rxpacket return status = %s, Bytes rx = %d",RX_STATUS_TEXT[status], nLength);

	//trace_printf("int rxpacket(...) end");
	//trace_printf("%u ms\r\n", stopwatch_elapsed());

	//trace_printf("*recvcount=nLength\r\n");
	//trace_printf("%u ms\r\n", stopwatch_elapsed());
	//stopwatch_stop();
	//trace_printf("rxpacket %u ms\r\n", stopwatch_elapsed());
	//logg(LOG_INFO |  LOG_DEXLOG,"%u ms", stopwatch_elapsed());
    return status;
}

//////////////////////////////////////////////////////////////
#define WORKBUF_SIZE 512
char workbuf[WORKBUF_SIZE];

int dex_status;
int dexLength; // will be set by ReadDex routine
char password[]="000000";
///////////////////////////////////////////////////////////////

void ucs_readDexMethod()
{
	char r[15];
	dex_status = 0;
	dexLength = 0;
	//trace_printf("\r\nStarting to Read DEX ...\r\n");
        uint8_t b = settings_storage.led_brightness;
	rgbled_set(0,0,b);

        serial_wakeup();
	UCS_Initialize();        // must be done before any other api call


	dex_status = UCS_ReadDex(&dexLength, workbuf, WORKBUF_SIZE, password);

	rgbled_set(0,0,0);
	sprintf(r,"[xds%02d]", dex_status);
	
	#if defined USE_BLE
	ble_write_bytes(r, 7);
	ble_do_events();
	ble_write(0x04);
	ble_do_events();
	delay(10);
	#else
	//#error "User is stoopid!"
	#endif
	

	sprintf(r,"[xdl%05d]", dexLength);
	
	#if defined USE_BLE
	ble_write_bytes(r, 10);
	ble_do_events();
	ble_write(0x04);
	ble_do_events();
	delay(10);
	#else
	//#error "User is stoopid!"
	#endif
	

	if(dex_status == RX_OK)
	{
		//trace_printf("\r\nRead DEX succeeded.  Length=%d.\r\n", dexLength);
		rgbled_set(0,settings_storage.led_brightness,0);
		delay(250);
		rgbled_set(0,0,0);
		delay(250);
		rgbled_set(0,settings_storage.led_brightness,0);
		delay(250);
	}
	else
	{
		//trace_printf("Read DEX failed.  Error = %d.\r\n", dex_status);
		rgbled_set(settings_storage.led_brightness, 0, 0);
		delay(250);
		rgbled_set(0,0,0);
		delay(250);
		rgbled_set(settings_storage.led_brightness, 0, 0);
		delay(250);

	}
	rgbled_set(0,0,0);
	delay(250);
	rgbled_set(0, 0, settings_storage.led_brightness);
}

int ucs_readDexData(uint32_t address, uint32_t length)
{
  /*
	int cpart = dexLength/12;
	int mpart = dexLength%12;
	for(int i=0; i<cpart; i++)
	{
		led_toggle();
		memory_readBytes(i*12, workbuf, 12);
		
		#if defined USE_BLE
                ble_write_bytes("*sheldonbbt*",12);
                ble_do_events();
                ble_write(0x04);
                ble_do_events();
		//ble_write_bytes(workbuf, 12);
		ble_do_events();
		#else
		//#error "User is stoopid!"
		#endif
	}
        
	memory_readBytes(dexLength-mpart, workbuf, mpart);
	
	#if defined USE_BLE
	//ble_write_bytes(workbuf, mpart);
        ble_write_bytes("*ble*",5);
        ble_do_events();
        ble_write(0x04);
        ble_do_events();
	ble_do_events();
	#else
	//#error "User is stoopid!"
	#endif*/
        int cpart = dexLength/12;
	int mpart = dexLength%12;
	for(int i=0; i<cpart; i++)
	{
		led_toggle();
                
		memory_readBytes(i*12, workbuf, 12);
		
		//ble_write_bytes("*sheldonbbt*",12);
                //ble_do_events();
                
		ble_write_bytes(workbuf, 12);
		ble_do_events();
		
	}
        
        
	memory_readBytes(dexLength-mpart, workbuf, mpart);
	
	//ble_write_bytes("*ble*",5);
        //ble_do_events();
        
	ble_write_bytes(workbuf, mpart);
	ble_do_events();
	
	return 0;
}
