/*
 * crc.h
 *
 * Created: 24/02/2014 21:49:57
 *  Author: VendWatch
 */ 


#ifndef CRC_H_
#define CRC_H_

unsigned short calc_crc(unsigned char *sBuffer, int iBufferLen);
unsigned short calc_crc_with_feed(unsigned char *sBuffer, int iBufferLen, unsigned short usCrc);

#endif /* CRC_H_ */