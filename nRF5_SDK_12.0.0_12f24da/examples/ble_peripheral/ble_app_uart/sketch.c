#include "sketch.h"

#include "rgbled.h"

#include "serial.h"
#include "ble_uart.h"
#include "command_processor.h"

#include "ucs.h"
#include "battery.h"
#include "fuelgauge.h"
#include "usb.h"
#include "accelerometer.h"

#include "sleepmgr.h"
#include "eeprom.h"

#include "pwm.h"
#include "WMath.h"

#include "SEGGER_RTT.h"

double pwm_max2 = 4095;
double valBrightness2;
double redBrightness2;
double greenBrightness2;
double blueBrightness2;
double rgbMillis2 = 0;
double rB = 0;
double gB = 0;
double bB = 0;
bool isBreathing2 = false;
double charge2 = 0;
char device_name[15]="SnailBLE";
char serial_number[14]="              ";

void setup(void)
{
    NRF_POWER->GPREGRET = 0xB0;
    
    // Begin Snail Code
    settings_storage.battery_level = 100;
    settings_storage.led_brightness = 50;
    settings_storage.led_red = 0;
    settings_storage.led_green = 0;
    settings_storage.led_blue = 50;
    settings_storage.time_to_sleep = 18;
    settings_storage.time_to_reconnect = 0;
    settings_storage.wake_on_shake_sensitivity = 10;
    settings_storage.wake_on_serial_switch = 1;
    settings_storage.wake_on_charging = 0;
    settings_storage.bootloader_switch = 0;

    char version[10];
    strcpy(version, "1.2.5.005");
    settings_storage.version_of_program[0] = version[0];
    settings_storage.version_of_program[1] = version[1];
    settings_storage.version_of_program[2] = version[2];
    settings_storage.version_of_program[3] = version[3];
    settings_storage.version_of_program[4] = version[4];
    settings_storage.version_of_program[5] = version[5];
    settings_storage.version_of_program[6] = version[6];
    settings_storage.version_of_program[7] = version[7];
    settings_storage.version_of_program[8] = version[8];
    settings_storage.version_of_program[9] = version[9];
    
    //294100ccbb
    //%02x%02x%02x%02x%02x
    
    //"SL%02x%02x%02x%02x%02x%02x
    /*sprintf(serial_number, "SL%02x%02x%02x%02x%02x%02x", 
            eeprom_read(0xfa),
            eeprom_read(0xfb),
            eeprom_read(0xfc),
            eeprom_read(0xfd),
            eeprom_read(0xfe),
            eeprom_read(0xff)
           );*/
    
    sprintf(serial_number, "%c%c%c%c", 
            //eeprom_read(0xfa),
            //eeprom_read(0xfb),
            eeprom_read(0xfc),
            eeprom_read(0xfd),
            eeprom_read(0xfe),
            eeprom_read(0xff)
           );
    

    rgbled_begin();

    rgbled_set(50, 0, 0);
    delay(250);
    rgbled_set(0, 0, 0);
    delay(250);
    settings_save_config();
    rgbled_set(50, 0, 0);
    delay(250);
    rgbled_set(0, 0, 0);
    delay(250);

    delay(100);

    rgbled_set(0, 50, 0);
    delay(250);
    rgbled_set(0, 0, 0);
    delay(250);
    settings_load_config();
    rgbled_set(0, 50, 0);
    delay(250);
    rgbled_set(0, 0, 0);
    delay(250);

    delay(100);
    rgbled_set(50, 0, 0);
    delay(250);
    rgbled_set(0, 0, 0);
    delay(250);
    accelerometer_shakeMode(settings_storage.wake_on_shake_sensitivity, true, true, true, false);
    rgbled_set(50, 0, 0);
    delay(250);
    rgbled_set(0, 0, 0);
    delay(250);

    delay(100);

    usb_begin();

    delay(10);

    rgbled_set(0, 50, 0);
    delay(250);
    rgbled_set(0, 0, 0);
    delay(250);
    fuelgauge_begin();
    fuelgauge_reset();
    fuelgauge_quickStart();
    rgbled_set(0, 50, 0);
    delay(250);
    rgbled_set(0, 0, 0);
    delay(250);

    delay(10);
    pinMode(SERIALPORT_PIN_RX, HIGH_Z);
    serial_begin();
    delay(25);
    serial_end();
    delay(25);
    ble_begin(device_name, serial_number);
    delay(25);
    ble_end();
    delay(25);
    // End Snail Code
    
    SEGGER_RTT_WriteString(0, "Hello World!\n");
}


void loop(void)
{  
  
    // Begin Snail Code
    if(accelerometer_shaked())
    {
        
        //rgbled_set(0, 127, 0);
        //rgbled_set(0, settings_storage.led_brightness, 0);
        showBatteryLevelStored(settings_storage.led_brightness);
        stopwatch_stop();
        stopwatch_reset();
        stopwatch_start();
        
        long time_sleep = settings_storage.time_to_sleep*1000L;
        long time_reconnect = settings_storage.time_to_reconnect*1000L;
        long time_wait = time_sleep;
        //showBatteryStateNormal(settings_storage.led_brightness);
        ble_begin(device_name, serial_number);
        
        if(accelerometer_shaked())
        {

        
            while(accelerometer_shaked() && !usb_attached() && stopwatch_elapsed() <= time_wait)
            {
                if(ble_connected())
                {
                  stopwatch_stop();
                  stopwatch_overflow();
                  while(ble_connected())
                  {
                    commandEvent();
                  }
                  goto lblcon;
                }
                
                // show battery level normal
                showBatteryStateNormal(settings_storage.led_brightness);
            }
            stopwatch_stop();
            stopwatch_overflow();
            
            goto lblend;
            ////////////////////////////////////////////////////////////////////////
 
        lblcon:
              
            stopwatch_stop();
            stopwatch_reset();
            stopwatch_start();
            time_reconnect = settings_storage.time_to_reconnect*1000L;
            time_wait = time_reconnect;
            while(accelerometer_shaked() && !usb_attached() && stopwatch_elapsed() <= time_wait)
            {
              
                if(ble_connected())
                {
                  stopwatch_stop();
                  stopwatch_overflow();
                  
                  while(ble_connected())
                  {
                    commandEvent();
                  }
                  
                  goto lblcon;
                }

                // show battery level normal
                showBatteryStateNormal(settings_storage.led_brightness);
            }
            
            stopwatch_stop();
            stopwatch_overflow();
            
            ////////////////////////////////////////////////////////////////////////
        lblend:
            rgbled_set(0, 0, 0);
           
            
        }
        
        if(usb_attached()) 
        {
            rgbled_set(0, 0, 0);
            //showBatteryLevelStored(settings_storage.led_brightness);
            
            //showBatteryStateNormal(settings_storage.led_brightness);
            // if ble wake on charging
            if(settings_storage.wake_on_charging == 1) {
                ble_begin(device_name, serial_number);
            }
            else
            {
                // do nothing!
            }
            
            rgbMillis2 = 16123.0;
            isBreathing2 = true;
                
            while(usb_attached())
            {
                if(settings_storage.wake_on_charging == 1) 
                {
                    commandEvent();
                }
                else
                {
                    // do nothing!
                }

                // show battery level charging
                do{}while(fuelgauge_getSoC()<=0.0f);
                charge2 = (double)fuelgauge_getSoC();
                if(charge2>100.0f)charge2=100.0f;
                settings_storage.battery_level = charge2;
                //settings_save_config();
                //settings_load_config();

                valBrightness2 =  (( exp(sin(rgbMillis2/7000.0*PI)) - 0.36787944 ) * 27756.0)/65535.0;

                if(charge2<BATT_MIN_CHARGING)
                {
                    redBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095);
                    greenBrightness2 = valBrightness2 * 0;
                    blueBrightness2 = valBrightness2 * 0;
                }

                if(charge2>=BATT_MIN_CHARGING && charge2<BATT_MAX_CHARGING)
                {
                    redBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095)/2;
                    greenBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095)/2;
                    blueBrightness2 = valBrightness2 * 0;
                }

                if(charge2>=BATT_MAX_CHARGING)
                {
                    redBrightness2 = valBrightness2 * 0;
                    greenBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095);
                    blueBrightness2 = valBrightness2 * 0;
                }
                
                rgbled_setRAW((uint32_t)redBrightness2, (uint32_t)greenBrightness2, (uint32_t)blueBrightness2);
                rgbMillis2 = rgbMillis2 + 3.8;
                delayMicroseconds(500);
            }
            
            if(isBreathing2)
            {
                while(valBrightness2*(float)pwm_max2 > 1.0)
                {
                  valBrightness2 =  (( exp(sin(rgbMillis2/7000.0*PI)) - 0.36787944 ) * 27756.0)/65535.0;
                  if(charge2<BATT_MIN_CHARGING)
                  {
                      redBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095);
                      greenBrightness2 = valBrightness2 * 0;
                      blueBrightness2 = valBrightness2 * 0;
                  }

                  if(charge2>=BATT_MIN_CHARGING && charge2<BATT_MAX_CHARGING)
                  {
                      redBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095)/2;
                      greenBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095)/2;
                      blueBrightness2 = valBrightness2 * 0;
                  }

                  if(charge2>=BATT_MAX_CHARGING)
                  {
                      redBrightness2 = valBrightness2 * 0;
                      greenBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095);
                      blueBrightness2 = valBrightness2 * 0;
                  }
                  rgbled_setRAW((uint32_t)redBrightness2, (uint32_t)greenBrightness2, (uint32_t)blueBrightness2);
                  rgbMillis2 = rgbMillis2 + 3.8;
                  delayMicroseconds(500);
                }
                rgbled_set(0, 0, 0);
                isBreathing2 = false;
                rgbMillis2 = 10500;
                
            }
            rgbled_set(0, 0, 0);
        }
        
        if(accelerometer_shaked())
        {    
            accelerometer_clearInterrupt();
            rgbled_set(0, 0, 0);
            rgbMillis2 = 10500.0;
            isBreathing2 = false;
        }
        
        
    }
    
    while(serial_attached() || usb_attached()) 
    {
      
      if(usb_attached())
      {
          //rgbled_set(0, 0, 0);
          
          stopwatch_stop();
          stopwatch_reset();
          stopwatch_start();
          
          if(usb_attached()) 
          {
              rgbled_set(0, 0, 0);
              //showBatteryLevelStored(settings_storage.led_brightness);
              //showBatteryStateNormal(settings_storage.led_brightness);
              
              // if ble wake on charging
              if(settings_storage.wake_on_charging == 1) {
                  ble_begin(device_name, serial_number);
              }
              else
              {
                  // do nothing!
              }
              
              rgbMillis2 = 10500.0;
              isBreathing2 = true;
                  
              while(usb_attached())
              {
                  if(settings_storage.wake_on_charging == 1) 
                  {
                      commandEvent();
                  }
                  else
                  {
                      // do nothing!
                  }

                  // show battery level charging
                  do{}while(fuelgauge_getSoC()<=0.0f);
                  charge2 = (double)fuelgauge_getSoC();
                  if(charge2>100.0f)charge2=100.0f;
                  settings_storage.battery_level = charge2;
                  //settings_save_config();
                  //settings_load_config();

                  valBrightness2 =  (( exp(sin(rgbMillis2/7000.0*PI)) - 0.36787944 ) * 27756.0)/65535.0;

                  if(charge2<BATT_MIN_CHARGING)
                  {
                      redBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095);
                      greenBrightness2 = valBrightness2 * 0;
                      blueBrightness2 = valBrightness2 * 0;
                  }

                  if(charge2>=BATT_MIN_CHARGING && charge2<BATT_MAX_CHARGING)
                  {
                      redBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095)/2;
                      greenBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095)/2;
                      blueBrightness2 = valBrightness2 * 0;
                  }

                  if(charge2>=BATT_MAX_CHARGING)
                  {
                      redBrightness2 = valBrightness2 * 0;
                      greenBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095);
                      blueBrightness2 = valBrightness2 * 0;
                  }
                  
                  rgbled_setRAW((uint32_t)redBrightness2, (uint32_t)greenBrightness2, (uint32_t)blueBrightness2);
                  rgbMillis2 = rgbMillis2 + 3.8;
                  delayMicroseconds(500);
              }
              
              if(isBreathing2)
              {
                  while(valBrightness2*(float)pwm_max2 > 1.0)
                  {
                    valBrightness2 =  (( exp(sin(rgbMillis2/7000.0*PI)) - 0.36787944 ) * 27756.0)/65535.0;
                    if(charge2<BATT_MIN_CHARGING)
                    {
                        redBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095);
                        greenBrightness2 = valBrightness2 * 0;
                        blueBrightness2 = valBrightness2 * 0;
                    }

                    if(charge2>=BATT_MIN_CHARGING && charge2<BATT_MAX_CHARGING)
                    {
                        redBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095)/2;
                        greenBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095)/2;
                        blueBrightness2 = valBrightness2 * 0;
                    }

                    if(charge2>=BATT_MAX_CHARGING)
                    {
                        redBrightness2 = valBrightness2 * 0;
                        greenBrightness2 = valBrightness2 * map(settings_storage.led_brightness, 0, 255, 0, 4095);
                        blueBrightness2 = valBrightness2 * 0;
                    }
                    rgbled_setRAW((uint32_t)redBrightness2, (uint32_t)greenBrightness2, (uint32_t)blueBrightness2);
                    rgbMillis2 = rgbMillis2 + 3.8;
                    delayMicroseconds(500);
                  }
                  isBreathing2 = false;
                  rgbMillis2 = 10500;
                  rgbled_set(0, 0, 0);
              }
              isBreathing2 = false;
              rgbMillis2 = 10500;
              rgbled_set(0, 0, 0);
              delay(50);
          }
          
          if(accelerometer_shaked())
          {    
              accelerometer_clearInterrupt();
              isBreathing2 = false;
              rgbMillis2 = 10500;
              rgbled_set(0, 0, 0);
          }
      }
      
      if(serial_attached())
      {
          //rgbled_set(0, 0, 0);
          
          stopwatch_stop();
          stopwatch_reset();
          stopwatch_start();
          
          if(serial_attached())
          {
              showBatteryLevelStored(settings_storage.led_brightness);
              // if ble wake on charging
              if(settings_storage.wake_on_serial_switch == 1) 
              {
                  ble_begin(device_name, serial_number);
              }
              else
              {
                  // do nothing!
              }
              while(serial_attached())
              {
                  commandEvent();
                  // show battery level normal
                  showBatteryStateNormal(settings_storage.led_brightness);
              }
              isBreathing2 = false;
              rgbMillis2 = 10500;
              rgbled_set(0, 0, 0);
              delay(50);
          }
          
          if(accelerometer_shaked())
          {    
              accelerometer_clearInterrupt();
              isBreathing2 = false;
              rgbMillis2 = 10500;
              rgbled_set(0, 0, 0);
          }
      }
    
    }
    
    //rgbled_set(0, 0, 0);
    //ble_end();
    // sleep
    sleepmgr_sleep();
    // End Snail Code
}