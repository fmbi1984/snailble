#include "eeprom.h"

#include "config.h"
#include "sdk_config.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "app_uart.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"
#include "app_error.h"
#include "nrf.h"
#include "bsp.h"
#include "app_util_platform.h"
#include "nrf_delay.h"



static const nrf_drv_twi_t m_twi_master = NRF_DRV_TWI_INSTANCE(MASTER_TWI_INST);
static unsigned char i2c_address;

////////////////////////////////////////////////////////////////////////////////
static void i2c_begin(void)
{
    ret_code_t ret;
    const nrf_drv_twi_config_t config =
    {
       .scl                = TWI_SCL_M,
       .sda                = TWI_SDA_M,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH
    };
    nrf_drv_twi_uninit(&m_twi_master);
    do
    {
        ret = nrf_drv_twi_init(&m_twi_master, &config, NULL, NULL);
        if(NRF_SUCCESS != ret)
        {
            break;
        }
        nrf_drv_twi_enable(&m_twi_master);
    }while(0);
    //return ret;
}

static void i2c_end()
{
  nrf_drv_twi_disable(&m_twi_master);
  nrf_drv_twi_uninit(&m_twi_master);
}

static unsigned char i2c_start(unsigned char address)
{
  i2c_address = address;
  return 0;
}

static void delay(unsigned long ms)
{
  nrf_delay_ms(ms);
}

static long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

////////////////////////////////////////////////////////////////////////////////

void eeprom_begin()
{
  i2c_begin();
}

void eeprom_end()
{
  i2c_end();
}

void eeprom_write(unsigned int eeaddress, byte data) 
{
  
  uint8_t tx_data[8];
  uint8_t tx_len;

  i2c_begin();

  i2c_address = EEPROM_ADDRESS;
  //tx_data[0] = (int)(eeaddress >> 8);   // MSB
  //tx_data[1] = (int)(eeaddress & 0xFF); // LSB
  tx_data[0] = (int) eeaddress;
  tx_data[1] = data; // data
  tx_len = 2;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, false); // stop transmitting & hang up
  nrf_delay_ms(5);
  //delay(5);
  
}
 
byte eeprom_read(unsigned int eeaddress) 
{
  
  uint8_t tx_data[8];
  uint8_t tx_len;
  
  uint8_t rx_data[8];
  uint8_t rx_len;
  
  byte rdata = 0xFF;
  
  i2c_begin();

  i2c_address = EEPROM_ADDRESS;
  //tx_data[0] = (int)(eeaddress >> 8);   // MSB
  //tx_data[1] = (int)(eeaddress & 0xFF); // LSB
  tx_data[0] = (int) eeaddress;
  tx_len = 1;
  nrf_drv_twi_tx(&m_twi_master, i2c_address, tx_data, tx_len, false); // stop transmitting & hang up
  
  rx_len = 1;
  nrf_drv_twi_rx(&m_twi_master, i2c_address, rx_data, rx_len);
  rdata = rx_data[0];
 
  return rdata;
}