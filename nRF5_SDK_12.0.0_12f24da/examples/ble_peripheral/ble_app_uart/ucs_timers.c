/*
 * ucs_timers.c
 *
 * Created: 24/02/2014 22:00:41
 *  Author: VendWatch
 */

#include "ucs_timers.h"

#include "wiring.h"
#include "wiring_digital.h"
//#include "wiring_analog.h"

/*
** Add whatever code is necessary to run millisecond resolution timers
*/

void initializeTimers()
{
}

/*
** This is all brute force.  If your implementation supports msec precision timers,
** replace this code with real timer calls.
*/

unsigned long getCurrentMsecs()
{
	return millis();		// replace with the actual milliseconds counter
}

void startTimer(TIMER* tmr, unsigned long duration)  // duration is ms
{
	*tmr = duration + getCurrentMsecs();
}

int timerExpired(TIMER* tmr)
{
	return (*tmr > getCurrentMsecs()) ? 0 : 1;
}

/*
** replace this with a call to a real sleep function, if one is available
*/
void msec_delay(unsigned long msecs)
{
	TIMER t = msecs + getCurrentMsecs();
	while(getCurrentMsecs() < t) ;
}
