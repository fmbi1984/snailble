#include "wiring_digital.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "app_uart.h"
#include "app_error.h"

#include "nrf.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"

void pinMode(uint8_t pin, uint8_t mode)
{
  if (mode == OUTPUT) {
    nrf_gpio_cfg_output(pin);
  }
  else if (mode == INPUT) {
    nrf_gpio_cfg_sense_input(pin, NRF_GPIO_PIN_NOPULL, NRF_GPIO_PIN_SENSE_HIGH);
  }
  else if (mode == INPUT_PULLDOWN) {
    nrf_gpio_cfg_sense_input(pin, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_SENSE_HIGH);
  }
  else if (mode == INPUT_PULLUP) {
    nrf_gpio_cfg_sense_input(pin, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_HIGH);
  }
  else if(mode == HIGH_Z)
  {
    NRF_GPIO->PIN_CNF[pin] = GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos;
  }
  
}

void digitalWrite(uint8_t pin, uint8_t val)
{
  nrf_gpio_pin_write(pin,val);
}

void digitalToggle(uint8_t pin)
{
  nrf_gpio_pin_toggle(pin);
}

int digitalRead(uint8_t pin)
{
  return nrf_gpio_pin_read(pin);
}
