#ifndef EEPROM_H_
#define EEPROM_H_

#include "wiring.h"

#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>

#define EEPROM_ADDRESS 0x50 //A0   //Address of 24LC256 eeprom chip

/*
#define VCELL_REGISTER		0x02
#define SOC_REGISTER		0x04
#define MODE_REGISTER		0x06
#define VERSION_REGISTER	0x08
#define CONFIG_REGISTER		0x0C
#define COMMAND_REGISTER	0xFE
*/

void eeprom_begin();
void eeprom_write(unsigned int eeaddress, byte data);
byte eeprom_read(unsigned int eeaddress);


#endif /* EEPROM_H_ */