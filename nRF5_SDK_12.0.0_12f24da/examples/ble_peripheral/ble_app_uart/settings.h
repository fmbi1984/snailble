/*
 * settings.h
 *
 * Created: 20/02/2014 10:30:19
 *  Author: Francisco Betancourt
 */ 


#ifndef SETTINGS_H_
#define SETTINGS_H_

// ID of the settings block
#define CONFIG_VERSION "ls1"

// Tell it where to store your config data in EEPROM
#define CONFIG_START 0 //200

// Example settings structure
struct StoreStruct {
    // The variables of your settings
    unsigned int led_brightness;
    unsigned int led_red;
    unsigned int led_green;
    unsigned int led_blue;
    unsigned int wake_on_shake_sensitivity;
    unsigned int wake_on_serial_switch;
    unsigned int wake_on_charging;
    unsigned int bootloader_switch;
    long time_to_sleep;
    long time_to_reconnect;
    double battery_level;
    // This is for mere detection if they are your settings
    char version_of_program[10]; // it is the last variable of the struct
    // so when settings are saved, they will only be validated if
    // they are stored completely.
};

extern struct StoreStruct settings_storage;

void settings_load_config(void);
void settings_save_config(void);

#endif /* SETTINGS_H_ */