/*
 * ucs.h
 *
 * Created: 24/02/2014 21:53:22
 *  Author: VendWatch
 */


#ifndef UCS_H_
#define UCS_H_

#include <stdint.h>
#include "crc.h"
#include "string.h"
#include "ucs_serial.h"
#include "ucs_timers.h"
#include "ucs_log.h"

#ifndef ucsFALSE
#define ucsFALSE 0
#endif

#ifndef ucsTRUE
#define ucsTRUE 1
#endif

#define utrl() numCharsWaiting(DEX_PORT)

#define C_ACK0  0
#define C_ACK1  1

#define C_SLAVE		0
#define C_MASTER	1

#define C_RESPONSE_TIMEOUT	3000
#define C_RX_TIMEOUT		250
#define C_NO_ACTIVITY_TIMEOUT	3000
#define C_MAX_UCS_RETRYS	10
///////////////////////////////////////////////////////////////////////////////
// This defines the maximum number of bytes of data bytes that can go into a
// block.  Note, the SPEC states this is 245; however, the 245 value includes
// the closing ETB or ETX for the block.  We use 244 so we can use this
// to determine how many data bytes we send per block.
///////////////////////////////////////////////////////////////////////////////
#define C_UCS_DL_MTU			244
#define C_DEX_START_SIZE		(41)	// strlen("DXS*1111111111*VA*V1/1*1**\r\nST*001*0001\r\n")
#define C_DEX_END_SIZE			(23)	// strlen("\r\nSE*50*0001\r\nDXE*1*1\r\n")
#define C_DEX_BLOCK_DATA_SIZE	(C_UCS_DL_MTU - (C_DEX_START_SIZE+C_DEX_END_SIZE))

#define HANDSHAKEBUFFERSIZE 40

// dex control characters

#define C_DLE 0x10
#define C_ENQ 0x05
#define C_STX 0x02
#define C_SOH 0x01
#define C_EOT 0x04
#define C_ETB 0x17
#define C_ETX 0x03
#define C_SYN 0x16
#define C_WACK 0x3b

/*
** How many times will we send ENQ and not receive a response ?
** Should be no negative consequences for more tries if the machine is
** not responding, and we have seen one machine type that may not respond
** for 5 or 6 tries.
*/

#define MAX_ENQ_TRIES 4
#define MAX_ENQ_TRIES_EXTENDED 10

#define MAX_TCP_RETRY_COUNT 4

//rxpacket return values

#define GPRS_ERROR		0
#define GPRS_OK 1
#define RX_OK GPRS_OK
#define RX_TIMEOUT	2
#define RX_GOTEOT	3
#define RX_GOTENQ	4
#define RX_BADCRC	5
#define RX_TOOBIG	6
#define RX_NOCHARS	7
#define RX_NODXS	8
#define RX_NODXE	9
#define RX_DISABLED	10
#define RX_HANDSHAKE_FAILED	11
#define RX_SLAVE_RX_FAILED	12
#define RX_MASTER_RX_FAILED	13
#define RX_COMM_FAILED		14
#define RX_POWER_FAILED		15
#define RX_POWER_RESTORED	16
#define RX_BATTERY_FAILED	17
#define RX_REMOTE_IS_MASTER 18
#define RX_WRONGACK 19
#define RX_DEXDISCARDED 99

#define C_INVALID_PASSWORD	0
#define C_VALID_PASSWORD	1
#define C_PASSWORD_LENGTH	12

#define PACKETCACHESIZE 256
#define DEXLENGTH 1024UL*12UL//(1024UL*1UL) // 2KB

int UCS_Initialize(void);
int UCS_InitAcks(void);
int UCS_SendENQ(void);
int UCS_WaitForENQ(void);
int UCS_waitForAck(int acknum);		// acknum is 0 or 1
int UCS_InitHandShakes( char *respcode,char bSend );
int UCS_ReadDexData(
char* sTempBuffer,int nTempBufSize,int *pnDexLength);
int UCS_SendDexPacket(char* sDex);
int UCS_SendPassword(char* 	sDexPassword);
int UCS_SendMasterHandshake(char bSend, int allowRemoteMaster);
int UCS_SendSlaveHandshake(void);
int UCS_ReadDexSlave(
int*		pnDexLength,
char*		sTempBuffer,
int		nTempBufSize,
char*   password);
int UCS_ReadDex(
int*		pnDexLength,
char*		sTempBuffer,
int		nTempBufSize,
char*   password);
int UCS_send_new_password(char *oldpwd, char *newpwd);
int UCS_send_data(char *pwd, char *data);
int UCS_clear_resetable_fields(char *pwd);
int UCS_clear_errors(char *pwd);
int UCS_updateVMserials(char *pwd, char *vtpasswd,char *dexdata);
int rxpacket(char *buf, int buflen, int starttimeout,
int chartimeout, int *recvcount);

extern int dex_status;
extern int dexLength;        // will be set by ReadDex routine
void ucs_readDexMethod(void);
int ucs_readDexData(uint32_t address, uint32_t length);


#endif /* UCS_H_ */
