/*
 * ucs_timers.h
 *
 * Created: 24/02/2014 22:00:53
 *  Author: VendWatch
 */ 


#ifndef UCS_TIMERS_H_
#define UCS_TIMERS_H_

typedef unsigned long TIMER;

void initializeTimers(void);
void startTimer(TIMER* tmr,unsigned long duration);  // duarion is ms
int timerExpired(TIMER* tmr);
void msec_delay(unsigned long msecs);

#endif /* UCS_TIMERS_H_ */